# normalize.py
import numpy as np
from numpy.linalg import norm

def normalize(objs, z_ideal, z_nadir):
    """
    Normalization of objective vectors, as recommended by 
    [He18] Evolutionary Many-objective Optimization based on Dynamical Decomposition
    
    Input
    objs        (pop_size, m_objs) objs matrix
    z_ideal     (m_objs, ) ideal vector
    z_nadir     (m_objs, ) nadir vector
    
    Output
    norm_objs   (pop_size, m_objs) norm objs matrix
    """
    
    pop_size, m_objs = objs.shape

    # alias
    FX = objs
    Z_ideal = z_ideal.reshape((1, m_objs))
    Z_nadir = z_nadir.reshape((1, m_objs))
    
    # divisor
    div = Z_nadir - Z_ideal
    
    if np.any(div == 0):
        eps = np.finfo(float).eps                   # https://docs.scipy.org/doc/numpy/reference/generated/numpy.finfo.html
        mask = div == 0
        div[mask] = eps
    
    norm_objs = (FX - Z_ideal) / div
    
    return norm_objs.copy()

def linear_normalize(t_u, t_u_min, t_u_max, t_s_min=0, t_s_max=1):
    """
    Normalize an unscaled number t_u in the unscaled range [t_u_min, t_u_max] 
    to the scaled range [t_s_min, t_s_max].
    
    Check 
    Chapter 7. Performance Issues (Supervised Learning). 
    Equation (7.10)
    Computational Intelligence: An Introduction. 
    Engelbrecht.
    
    Input
    t_u         number, unscaled input
    t_u_min     number, lower bound of t_u
    t_u_max     number, upper bound of t_u
    t_s_min     number, lower bound of t_s
    t_s_max     number, upper bound of t_s
    
    Output
    t_s         number, scaled output
    
    """
    factor  = (t_u - t_u_min)/(t_u_max - t_u_min) 
    t_s     = factor * (t_s_max - t_s_min) + t_s_min
    
    return t_s


def get_unitary_matrix(w):
    """
    Return a matrix (norm_w) where each row (vector) has a norm of 1.
    
    Input
    w           (rows, cols) array
    
    Output
    unit_w      (rows, cols) array, with unitary vectors    
    """
    
    rows, cols = w.shape
    per_row = 1
    norma = norm(w, axis=per_row).reshape((rows, 1))
    
    unit_w = w / norma
    
    return unit_w.copy()
