# par_vfns.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
from dominance import compare_min, A_IS_DOM_BY_B
from concurrent.futures import ProcessPoolExecutor, as_completed


def vfns_block(pid, objs, block):
    """
    Calcula una parte de la matriz de dominancia R
    determinada por el arreglo de indices block
    
    Input
    objs            (pop_size, m_objs) objs matrix
    block           (2, ) int tuple, determina el rango de rows de R que seran procesados
    
    Output
    R_block         (rows, pop_size) int matrix, fraccion de la matriz de dominancia R
    """
    
    # debug
    #print("pid: %d, block: (%d, %d)" % (pid, block[0], block[1])) 
    
    indices = np.arange(block[0], block[1], dtype=int)
    
    pop_size, m_objs = objs.shape
    rows = len(indices)
    
    R_block = np.zeros((rows, pop_size), dtype=int)
    
    for row, i in enumerate(indices):
        for j in range(pop_size):
            
            if i == j:
                R_block[row, j] = 0
                continue
            
            a = objs[i]     # x_i
            b = objs[j]     # x_j
            
            result = compare_min(a, b)
            
            if result == A_IS_DOM_BY_B:
                
                # x_i is inferior (worser) than x_j
                R_block[row, j] = 1
                
                # nota: es R[row, j], no R[i, j] porque i > rows
                
    # sum per rows
    by_rows = 1
    r_block = R_block.sum(axis=by_rows)
    
    # get ranks for this block
    ranks_block = np.ones(r_block.shape, dtype=int)     # they are ones, ie, we assume all solutions belongs to second front
    ranks_block[r_block==0] = 0                         # these solutions belongs to first front
                                                        # r_block == 0 means 'get the solutions dominated by 0 solutions'
    
   
    return pid, R_block.copy(), r_block.copy(), ranks_block, block
    

def get_block(pid, rows, procs):
    
    bs = int(np.ceil(rows/procs))       # block size
    
    start = (pid)*bs
    end = (pid+1)*bs
    
    if end > rows:
        #print("reset: end from %d to %d" % (end, rows))
        end = rows
    
    #block = np.arange(start, end, dtype=int)
    
    block = (start, end)
    
    return block

def par_vfns(objs, procs=12):
    """
    Implementacion paralela (concurrente) de very fast non-dominated sorting
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.ones((pop_size, pop_size), dtype=int)*-1
    r = np.ones((pop_size, ), dtype=int)            # r[i]=j, objs[i] is dominated by (inferior than) j solutions
                                                    # if r[i]=0, objs[i] belongs to the first front
    ranks = np.ones(pop_size, dtype=int)            # we assume two ranks only: 
                                                    # 0: first front
                                                    # 1: other fronts
                                                    # if ranks[i]=0, objs[i] belongs to the first front
    
    futures = []
    
    with ProcessPoolExecutor() as manager:
        
        # submit futures
        for pid in range(procs):
            
            block = get_block(pid, pop_size, procs)
            
            futures.append(manager.submit(vfns_block, pid, objs, block))
    
    
        # wait for futures
        for f in as_completed(futures):
            
            pid, R_block, r_block, ranks_block, block = f.result()
            
            indices = np.arange(block[0], block[1], dtype=int)
            
            # update main matrices
            R[indices, :] = R_block.copy()
            r[indices] = r_block.copy()
            ranks[indices] = ranks_block.copy()
            
            
    # add check
    if (R == -1).any():
        print("Error in R matrix, some rows of objs were not processed")
            
            
    # add support for returing first front only
    
    
    return ranks.copy()
    
def par_filter_pop(objs, procs):
    
    ranks = par_vfns(objs, procs)
    
    # keep first front
    to_keep = ranks == 0

    first_front = objs[to_keep]
    
    return first_front.copy()

    
if __name__ == "__main__":
    
    """
    pid: 0, block: (0, 3), indices: [0 1 2]
    pid: 1, block: (3, 6), indices: [3 4 5]
    pid: 2, block: (6, 9), indices: [6 7 8]
    reset: end from 12 to 10
    pid: 3, block: (9, 10), indices: [9]
    """

    rows = 10
    procs = 4
    
    for pid in range(procs):
        
        block = get_block(pid, rows, procs)
        indices = np.arange(block[0], block[1], dtype=int)
        
        print("pid: %d, block: %s, indices: %s" % (pid, block, indices))
    
    
    
