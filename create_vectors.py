# create_vectors.py
from __future__ import print_function
from reference_vectors import create_reference_vectors
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def replace_zero_weigths(weights, eps=0.00001):
    """
    Replace zero weights given an input matrix
    
    Input
    weights     (pop_size, m_objs) array, weights matrix
    eps         new weight value
    
    Output
    W           (pop_size, m_objs) array, new weights matrix
    """
    
    # replace zero weights
    W           = weights.copy()
    to_keep     = weights == 0
    W[to_keep]  = eps           #0.00001               # recommended value [Ishibuchi16]
    
    return W.copy()
    

if __name__ == "__main__":
    
    # (dims, spacing h1, spacing h2, is_for_testing)
    params = (
            
            # for H=2
            ( 3, 12, 0),     # 
            ( 4, 7, 0),      # 
            ( 5, 6, 0),      # 
            ( 6, 4, 0),      # 
            ( 7, 4, 0),      # 
            ( 8, 3, 0),      # 
            (10, 3, 0),      #
             
            )
            
    
    for dims, h1, h2 in params:
            
    
        # create first layer
        ##w = create_simplex_vectors(dims, h1, add_eps=False)
        w = create_reference_vectors(dims, h1)
        
        layer_a_count = w.shape[0]
        layer_b_count = 0
        
        # create second layer (optional)
        if h2:
            
            ##v = create_simplex_vectors(dims, h2, add_eps=False)*0.5     # this factor (0.5) is necessary according to [Deb14], Fig 4.
            v = create_reference_vectors(dims, h2)*0.5     # this factor (0.5) is necessary according to [Deb14], Fig 4.
            merged = np.vstack((w, v))
            w = merged.copy()
            
            layer_b_count = v.shape[0]
            
        # check by rows
        expected = w.shape[0]; real = w.sum(axis=1).sum()
        print("n vectors:", w.shape[0])
        print("total sum:", w.sum(axis=1).sum())        # each row sums (approx) 1, so n rows sums (approx) n
        
        if (expected != real):
            print("ERROR (m_objs: %d, h1: %d, h2: %d)" % (dims, h1, h2))
        
        subfix = ""
        
        # weights with zeros
        filename = "weights_m_%d_zeros.txt" % dims
        # uncomment to save
        np.savetxt(filename, w, fmt="%.10f")
        print("creating", filename)
        
        # weights with no zeros
        nw = replace_zero_weigths(w)
        filename = "weights_m_%d.txt" % dims
        # uncomment to save
        np.savetxt(filename, nw, fmt="%.10f")
        print("creating", filename)
        print("layer_a: %d, layer_b: %d" % (layer_a_count, layer_b_count))
        
        ids = np.ones((layer_a_count+layer_b_count, ), dtype=int)
        ids[:layer_a_count] = 0
        
        filename = "weights_m_%d_ids.txt" % dims
        np.savetxt(filename, ids, fmt="%d")
        
        print("")
