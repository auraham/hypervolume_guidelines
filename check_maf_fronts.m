% check_maf_fronts.m
addpath('/home/auraham/bitbucket/platemo/Public');
addpath('/home/auraham/bitbucket/platemo/Operators');
addpath('/home/auraham/bitbucket/platemo/Problems/MaF');
addpath('/home/auraham/bitbucket/platemo/Problems/DTLZ');
addpath('/home/auraham/bitbucket/platemo/Algorithms/NSGA-II');

% list of mops
mops = {{'dtlz1', @DTLZ1},
        {'dtlz2', @DTLZ2},
        {'dtlz3', @DTLZ3},
        {'inv-dtlz1', @IDTLZ1},
        {'maf1', @MaF1},
        {'maf2', @MaF2},
        {'maf3', @MaF3},
        {'maf4', @MaF4},
        {'maf5', @MaF5},
        };
        
for k = 1:length(mops)

    mop = mops{k};
    
    mop_name = mop{1};
    mop_fn = mop{2};
    
    pop_size = 100;     % population size (approximate number of points in the Pareto front)
        
    fprintf("-- %s --\n", mop_name);
    
    for m_objs = [3 5 8 10]
    
        fprintf("m_objs=%d, ", m_objs);
    
        % evaluate mop
        rng(100); 
        
        % config
        glob = GLOBAL('-algorithm', @NSGAII, '-problem', mop_fn, '-N', 100, '-M', m_objs, '-evaluation', 30000);

        % get pareto front
        [PF] = mop_fn('PF', glob, pop_size);
        
        z_nadir = max(PF, [], 1);
        
        for i = 1:length(z_nadir)
            
            fprintf("%.4f ", z_nadir(i));
        
        end
        fprintf("\n");
        
    end
    
end
