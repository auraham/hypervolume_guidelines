# contrib_hv.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
import subprocess

# add rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.helpers import save_filtered_objs
from rocket.helpers import filter_by_reference_point
from rocket.helpers import filter_by_nds
from rocket.helpers import normalize
from rocket.pareto_fronts import get_front

def hv(objs, ref_point_str):
    """
    Return the hypervolume of a solution set
    """
    
    # create tmp file
    filename_objs = "objs.txt"
    filepath_objs = os.path.join("tmp", filename_objs)
    
    filename_hv = "hv.txt"
    filepath_hv = os.path.join("tmp", filename_hv)
    
    # save objs
    save_filtered_objs(objs, filepath_objs)

    # prepare command
    cmd = ["wfg", "-q", filepath_objs, *ref_point_str, "-o", filepath_hv]
        
    # debug
    #print(" ".join(cmd))

    # call wfg
    proc = subprocess.run(cmd, stdout=subprocess.PIPE)
    
    # get result
    hv = np.genfromtxt(filepath_hv)
    
    hv_raw, hv_norm = hv[0], hv[1]
    
    return hv_raw
    
def contrib_hv(objs, ref_point_str):
    """
    Return the hypervolume contribution of each solution in objs 
    """
    
    pop_size, m_objs = objs.shape
    contrib = np.zeros((pop_size, ))
    
    #import ipdb; ipdb.set_trace()
    
    # hv considering all solutions
    hv_full = hv(objs, ref_point_str)
    
    for i in range(pop_size):
        
        to_keep = np.ones((pop_size, ), dtype=bool)
        to_keep[i] = False
        
        pop_partial = objs[to_keep]
        
        # hv considering all solutions but one
        hv_partial = hv(pop_partial, ref_point_str)

        contrib[i] = hv_full - hv_partial

    return contrib.copy()

