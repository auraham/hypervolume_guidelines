% run_hv_seq.m
% This script is employed to compare the output of run_hv_seq.py with this one.

input_paths = {
"/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_0_t_300.txt.norm.filtered.matlab",
"/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_1_t_300.txt.norm.filtered.matlab",
"/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_2_t_300.txt.norm.filtered.matlab",
"/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_3_t_300.txt.norm.filtered.matlab",
"/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_4_t_300.txt.norm.filtered.matlab"
};

% filtered files in input
input_paths = {
"input/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_0_t_300.txt.norm.filtered.matlab",
"input/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_1_t_300.txt.norm.filtered.matlab",
"input/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_2_t_300.txt.norm.filtered.matlab",
"input/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_3_t_300.txt.norm.filtered.matlab",
"input/maf1/mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_4_t_300.txt.norm.filtered.matlab"
};

ref_point = [1.1 1.1 1.1];

for k = 1:length(input_paths)


    % load objs
    path = input_paths{k};
    objs = dlmread(path);
    
    % compute hypervolume
    r = HV(objs, 0, ref_point);
    
    fprintf("%.10f\n", r);
    
end

