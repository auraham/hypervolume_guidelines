# contrib_hv_m_2.py
# This script replicates figures from 
# [Ishibuchi17] Reference Point Specification in Hypervolume Calculation
# for Fair Comparison and Efficient Search
# specifically
# Fig 3

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import os, sys, argparse
from distutils.dir_util import mkpath
from shutil import copy2
import subprocess

# add rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.helpers import save_filtered_objs
from rocket.helpers import filter_by_reference_point
from rocket.helpers import filter_by_nds
from rocket.helpers import normalize
from rocket.pareto_fronts import get_front

def hv(objs, ref_point_str):
    """
    Return the hypervolume of a solution set
    """
    
    # create tmp file
    filename_objs = "objs.txt"
    filepath_objs = os.path.join("tmp", filename_objs)
    
    filename_hv = "hv.txt"
    filepath_hv = os.path.join("tmp", filename_hv)
    
    # save objs
    save_filtered_objs(objs, filepath_objs)

    # prepare command
    cmd = ["wfg", "-q", filepath_objs, *ref_point_str, "-o", filepath_hv]
        
    # debug
    #print(" ".join(cmd))

    # call wfg
    proc = subprocess.run(cmd, stdout=subprocess.PIPE)
    
    # get result
    hv = np.genfromtxt(filepath_hv)
    
    hv_raw, hv_norm = hv[0], hv[1]
    
    return hv_raw
    
def contrib_hv(objs, ref_point_str):
    """
    Return the hypervolume contribution of each solution in objs 
    """
    
    pop_size, m_objs = objs.shape
    contrib = np.zeros((pop_size, ))
    
    #import ipdb; ipdb.set_trace()
    
    # hv considering all solutions
    hv_full = hv(objs, ref_point_str)
    
    for i in range(pop_size):
        
        to_keep = np.ones((pop_size, ), dtype=bool)
        to_keep[i] = False
        
        pop_partial = objs[to_keep]
        
        # hv considering all solutions but one
        hv_partial = hv(pop_partial, ref_point_str)

        contrib[i] = hv_full - hv_partial

    return contrib.copy()


def plot_pop(objs, contrib, title):
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    ax.set_title(title)
    ax.set_xlim(0, 1.5)
    ax.set_ylim(0, 1.5)
    
    #ax.plot(objs[:, 0], objs[:, 1], marker="o", ms=8, c="#74CE74",
    #        mec="#252525", ls="none", markeredgewidth=0.15)
   
    pop_size, _ = objs.shape
    sf = 1000   # scale factor

    for i in range(pop_size):
        
        ax.plot(objs[[i], 0], objs[[i], 1], marker="o", ms=contrib[i]*sf, c="#CBEACB",
            mec="#252525", ls="none", markeredgewidth=0.15)
   
        ax.plot(objs[[i], 0], objs[[i], 1], marker="o", ms=8, c="#74CE74",
            mec="#252525", ls="none", markeredgewidth=0.15)
   
    plt.show()
    

if __name__ == "__main__":
    
    m_objs = 2
    
    for r in ("1.1", "1.25", "1.5"):
    
        ref_point_str = [r] * m_objs
        
        objs = np.array([
                        [0, 4],
                        [1, 3],
                        [2, 2],
                        [3, 1],
                        [4, 0],
                        ])
                        
        # normalize
        objs = objs / 4
                        
        # compute contrib
        contrib = contrib_hv(objs, ref_point_str)
        print("r: %4s, contrib: %s" % (r, contrib))
        
        # plot
        title = "$r = %s$" % r
        plot_pop(objs, contrib, title)
        
    """
    # ---- no normalization this is ok -----
    objs = np.array([
                    [0, 4],
                    [1, 3],
                    [2, 2],
                    [3, 1],
                    [4, 0],
                    ])
                    
    ref_point = ["5"] * m_objs
    r1 = hv(objs, ref_point)

    # ---- no normalization this is ok -----
    objs = np.array([
                    [0, 4],
                    [1, 3],
                    [2, 2],
                    [3, 1],
                    [4, 0],
                    ])
                    
    ref_point = ["5.5"] * m_objs
    r2 = hv(objs, ref_point)
    
    # ---- normalization this is ok -----
    objs = np.array([
                    [0, 4],
                    [1, 3],
                    [2, 2],
                    [3, 1],
                    [4, 0],
                    ])
    objs_norm = objs / 4
    ref_point = ["1.25"] * m_objs       # 5/4 = 1.25
    r3 = hv(objs_norm, ref_point)

    # ---- normalization this is ok -----
    objs = np.array([
                    [0, 4],
                    [1, 3],
                    [2, 2],
                    [3, 1],
                    [4, 0],
                    ])
    objs_norm = objs / 4
    ref_point = ["1.375"] * m_objs       # 5.5/4 = 1.375
    r4 = hv(objs_norm, ref_point)


    # ---- no normalization no solutio c = [2,2] this is -----
    objs = np.array([
                    [0, 4],
                    [1, 3],
                    #[2, 2],
                    [3, 1],
                    [4, 0],
                    ])
                    
    ref_point = ["5"] * m_objs
    r5 = hv(objs, ref_point)
    """
