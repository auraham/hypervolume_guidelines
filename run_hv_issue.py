# run_hv_issue.py
# %run run_hv.py -k rocket
from __future__ import print_function
import numpy as np
import os, sys, argparse
np.seterr(all='raise')

# add rocket 
# rev 3d085db of moead_mss_dci has the issue
# rev 9e9cfda of moead_mss_dci fixes the issue
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.performance import compute_hv_front
from rocket.performance import compute_hv_seq, merge_hv_files

if __name__ == "__main__":
    
    
    moea_name = "mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true"
    mop_name = "dtlz2"
    m_objs = 8
    runs = 1
    iters = 800
    experiment_path = "input"
    experiment_path_git = ""
    log_path = "log"

    compute_hv_front(               # example
        mop_name,                   # maf1
        m_objs,                     # 3
        experiment_path,            # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
        experiment_path_git,        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
        log_path
    )       
    
    compute_hv_seq(             # example
        moea_name,              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
        mop_name,               # maf1
        m_objs,                 # 3
        runs,                   # 50
        iters,                  # 300
        experiment_path,        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
        experiment_path_git,    # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
        log_path
    )

    merge_hv_files(             # example
        moea_name,              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
        mop_name,               # maf1
        m_objs,                 # 3
        runs,                   # 50
        iters,                  # 300
        experiment_path,        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
        experiment_path_git,    # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
        log_path
    )
