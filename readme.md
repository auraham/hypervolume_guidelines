# Guidelines for hypervolume calculation

This repository explains:



- `hypervolue.md` explains how to use `wfg` to compute the hypervolume indicator.
- `computing_hv_with_python.md` explains how to invoke `wfg` from python.
- `contrib_hv_m_2.py` and `contrib_hv_m_3.py` reproduces figures from [Ishibuchi17] and [Ishibuchi18]
- `ref_point_specification.md` summarizes a few notes from [Ishibuchi17] for specifying the reference vector for computing the hypervolume.
- `hypervolume_matlab_c.md` compares both implementations



## todo

- [ ] terminar ejemplo en 2 y 3 dims
- [ ] crear notas sobre la comparacion de ambos implementaciones (mostrar los ejemplos)
- [ ] agregar log a `run_hv_seq.py`
- [ ] modificar `run_hv_seq.py` para que no cree los archivos duplicados, sino que solo haga el consolidado y lo copie al directorio de backup
- [ ] probarlo en mi sandbox
- [ ] subirlo a `alux2`







## References

- [Ishibuchi17] *Reference Point Specification in Hypervolume Calculation for Fair Comparison and Efficient Search.*
- [Ishibuchi18] *How to Specify a Reference Point in Hypervolume Calculation for Fair Performance Comparison.*



## Contact