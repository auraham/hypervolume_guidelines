% create_pareto_front.m
% This script saves the pareto front of a given problem instance
addpath('/home/auraham/bitbucket/platemo/Public');
addpath('/home/auraham/bitbucket/platemo/Operators');
addpath('/home/auraham/bitbucket/platemo/Problems/MaF');
addpath('/home/auraham/bitbucket/platemo/Problems/DTLZ');
addpath('/home/auraham/bitbucket/platemo/Algorithms/NSGA-II');

% list of mops
mops = {{'maf1', @MaF1},
        {'maf2', @MaF2},
        {'maf3', @MaF3},
        {'maf4', @MaF4},
        {'maf5', @MaF5},
        {'maf6', @MaF6},
        {'maf7', @MaF7},
        {'maf8', @MaF8},
        {'maf9', @MaF9},
        {'maf10', @MaF10},
        {'maf11', @MaF11},
        {'maf12', @MaF12},
        {'maf13', @MaF13},
        {'maf14', @MaF14},
        {'maf15', @MaF15},
        {'inv-dtlz1', @IDTLZ1}
        };
        
% {m_objs, h, pop_size}
params = {
        {3, 12, 91},
        {5, 6, 210},
        {8, 3, 120},
        {10, 3, 220}
        };
        

% select a mop
mop = mops{2};     % maf2

for i = 1:length(params)

    % define params
    m_objs = params{i}{1};         % number of objectives
    pop_size = params{i}{3};       % population size (approximate number of points in the Pareto front)
           
    % -- dont edit from here

    mop_name = mop{1};
    mop_fn = mop{2};

    % set seed
    % rand('state', 100);   % for octave
    rng(100);               % for matlab: https://sachinashanbhag.blogspot.com/2012/09/setting-up-random-number-generator-seed.html

    % config
    glob = GLOBAL('-algorithm', @NSGAII, '-problem', mop_fn, '-N', 100, '-M', m_objs, '-evaluation', 30000);

    % get pareto front
    [PF] = mop_fn('PF', glob, pop_size);

    % plot fronts
    %figure;
    %Draw(PF);

    % save
    filename = sprintf('fronts/front_%s_m_%d.txt', mop_name, m_objs);
    dlmwrite(filename, PF, 'delimiter', ' ', 'precision', '%.30f');

    % print z_nadir
    z_nadir = max(PF, [], 1);
    fprintf("%s z_nadir: ", mop_name);
    for m = 1:length(z_nadir)
        fprintf("%.4f", z_nadir(m));
    end
    fprintf("\n");

    % debug
    fprintf("%s\n\n", filename);

end
