# create_nds_datasets.py
from __future__ import print_function
from reference_vectors import create_reference_vectors
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from numpy.random import RandomState

if __name__ == "__main__":
    
    params = (
        # (m_objs, H1, H2)
        (2, 9, 9),
        (3, 6, 7),
        )
    
    n_datasets = 1
    
    for m_objs, H1, H2 in params:
        
        # create layers
        first_layer  = create_reference_vectors(m_objs, H1) 
        second_layer = create_reference_vectors(m_objs, H2) * 100
        
        # merge layers
        merge = np.vstack((first_layer, second_layer))
        
        # indices
        n_points = merge.shape[0]
        indices = np.arange(n_points, dtype=int)
        ranks = np.ones((n_points, ), dtype=int)
        
        # set ranks
        # we have two ranks only: 0=first front, 1:second front
        a_points = first_layer.shape[0]
        ranks[:a_points] = 0
        
        # save points
        #filename = "dataset_m_%d_h1_%d_h2_%d.txt" % (m_objs, H1, H2, seed)
        filename = "pop_m_%d.txt" % m_objs
        np.savetxt(filename, merge)
        
        # save indices
        #filename = "dataset_m_%d_h1_%d_h2_%d_n_%d_indices.txt" % (m_objs, H1, H2, seed)
        #np.savetxt(filename, indices, fmt="%d")
        
        # save ranks
        #filename = "dataset_m_%d_h1_%d_h2_%d_n_%d_ranks.txt" % (m_objs, H1, H2, seed)
        filename = "pop_m_%d_ranks.txt" % m_objs
        np.savetxt(filename, ranks, fmt="%d")
        
        print(filename)
            
            
    # test
    objs = np.genfromtxt("pop_m_3.txt")
    ranks = np.genfromtxt("pop_m_3_ranks.txt", dtype=int)
    
    # first front
    to_keep = ranks == 0
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo", label="second front")
    ax.plot(objs[to_keep, 0], objs[to_keep, 1], objs[to_keep, 2], "go", label="first front")


    ax.legend(loc="upper right")
    ax.view_init(30, 45)

    plt.show()
