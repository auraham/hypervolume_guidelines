# maf4.py
from __future__ import print_function
from pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np
by_row = 1

def get_front_maf4(m_objs, h):
    """
    Pareto front of MaF4
    """
    
    """
    matlab
    f = UniformPoint(input,Global.M);
    f = f./repmat(sqrt(sum(f.^2,2)),1,Global.M);
    f = (1-f).*repmat(2.^(1:Global.M),size(f,1),1);
    """
    
    # uniform set of vectors
    f = simplex_lattice(m_objs, h)                      # (pop_size, m_objs)
    
    # shape
    pop_size, _ = f.shape
    
    
    # f = f./repmat(sqrt(sum(f.^2,2)),1,Global.M);
    #        A      B
    
    
    B = np.sqrt(np.sum(f**2, axis=by_row))              # (pop_size, )
    B = B.reshape((pop_size, 1))                        # (pop_size, 1)
    A = np.tile(B, [1, m_objs])                         # (pop_size, m_objs)
    f = f / A
    
    # f = (1-f).*repmat(2.^(1:Global.M),size(f,1),1);
    #            A      B
    
    B = np.array([[2**(p+1) for p in range(m_objs)]])   # 2**1, 2**2, ..., 2**m_objs
    A = np.tile(B, [pop_size, 1])                       # (pop_size, m_objs)
    f = (1 - f) * A                                     # (pop_size, m_objs)
    
    return f.copy()
