# simplex_lattice.py
from __future__ import print_function
from pareto_fronts.reference_vectors import create_reference_vectors
import numpy as np
from numpy.linalg import norm 
import os, sys

np.seterr(all='raise')

def simplex_lattice(m_objs, h):
    """
    Create a uniform set of weight vectors
    
    Input
    m_objs      int, number of objectives
    h           spacing parameter
    
    Output
    w           (n_points, m_objs) weight matrix
    """
    
    w = create_reference_vectors(m_objs, h)
    return w.copy()
    
    """
    # spacing H1, H2
    spacing = {
        2: (99, 0),
        3: (12, 0),
        5: (6, 0),
        8: (3, 0),
        10: (3, 0),
    }
    
    dims = m_objs
    h1, h2 = spacing[m_objs]
    
    # create first layer
    w = create_reference_vectors(dims, h1)
    
    # create second layer (optional)
    if h2:
        
        v = create_reference_vectors(dims, h2) * 0.5    # this factor (0.5) is necessary according to [Deb14], Fig 4.
        merged = np.vstack((w, v))
        w = merged.copy()
    
    return w.copy()
    """
        
