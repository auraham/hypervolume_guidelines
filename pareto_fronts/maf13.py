# maf13.py
from __future__ import print_function
from pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np
by_col = 0
by_row = 1

def get_front_maf13(m_objs, h):
    """
    Pareto front of MaF13
    """
    
    """
    matlab
    f = UniformPoint(input,3);
    f = f./repmat(sqrt(sum(f.^2,2)),1,3);
    f = [f,repmat(f(:,1).^2+f(:,2).^10+f(:,3).^10,1,Global.M-3)];
    """
    
    # uniform set of vectors
    f = simplex_lattice(3, h)                           # (pop_size, 3)
    
    # shape
    pop_size, _ = f.shape
    
    
    # f = f./repmat(sqrt(sum(f.^2,2)),1,3);
    #        A      B                   
    
    
    B = np.sqrt(np.sum(f**2, axis=by_row))              # (pop_size, )
    B = B.reshape((pop_size, 1))                        # (pop_size, 1)
    A = np.tile(B, [1, 3])                              # (pop_size, 3)
    f = f / A
    
    
    # added to increase precision
    # and avoid an underflow (power 10 next line)
    f = np.array(f, dtype=np.longdouble)
    
    # f = [f,repmat(f(:,1).^2+f(:,2).^10+f(:,3).^10,1,Global.M-3)];
    #        A      col_a     col_b      col_c            
    
    col_a = f[:, 0]**2
    col_b = f[:, 1]**10
    col_c = f[:, 2]**10
    suma = col_a + col_b + col_c                        # (pop_size, )
    suma = suma.reshape((pop_size, 1))
    A = np.tile(suma, [1, m_objs - 3])                  # (pop_size, m_objs-3)
    f = np.hstack((f, A))                               # (pop_size, m_objs)
    
    
    return f.copy()
