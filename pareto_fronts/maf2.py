# maf2.py
from __future__ import print_function
from pareto_fronts.reference_vectors import create_reference_vectors
from numpy.linalg import norm 
import numpy as np
by_col = 0
by_row = 1

def print_matrix(mat):
    
    for row in mat:
        print(" ".join(["%.4f" % val for val in row]))

def simplex_lattice_maf2(m_objs, h):
    """
    Like simplex_lattice() but with more vectors
    Create a uniform set of weight vectors
    
    Input
    m_objs      int, number of objectives
    
    Output
    w           (n_points, m_objs) weight matrix
    """
    
    # notice that we now use h, instead of the spacing dict
    # create first layer
    w = create_reference_vectors(m_objs, h)
    return w
    
    """
    # spacing H1, H2
    spacing = {
        2: (99, 0),
        3: (37, 0),         # 130 puntos
        5: (15, 0),         #  86 puntos
        8: (3, 0),          # 120 puntos
        10: (3, 0),         # 220 puntos
    }
    
    dims = m_objs
    h1, h2 = spacing[m_objs]
    
    # create first layer
    w = create_reference_vectors(dims, h1)
    
    # create second layer (optional)
    if h2:
        
        v = create_reference_vectors(dims, h2) * 0.5    # this factor (0.5) is necessary according to [Deb14], Fig 4.
        merged = np.vstack((w, v))
        w = merged.copy()
    
    return w.copy()
    """

def get_front_maf2(m_objs, h):
    """
    Pareto front of MaF2
    """
    
    """
    matlab
    M = Global.M;
    f = max(UniformPoint(input,M),1e-6);
    c = zeros(size(f,1),M-1);
    for i = 1 : size(f,1)
        for j = 2 : M
            temp = f(i,j)/f(i,1)*prod(c(i,M-j+2:M-1));
            c(i,M-j+1) = sqrt(1/(1+temp^2));
        end
    end
    if M > 5
        c = c.*(cos(pi/8)-cos(3*pi/8)) + cos(3*pi/8);
    else
        c(any(c<cos(3*pi/8)|c>cos(pi/8),2),:) = [];
    end
    f = fliplr(cumprod([ones(size(c,1),1),c(:,1:M-1)],2)).*[ones(size(c,1),1),sqrt(1-c(:,M-1:-1:1).^2)];
    varargout = {f};
    """
    
    # M = Global.M;
    # f = max(UniformPoint(input,M),1e-6);
    
    M = m_objs
    f = np.maximum(simplex_lattice_maf2(M, h), 1e-6)
    
    # just for comparison
    # debug
    # create this file with platemo/create_pareto_front.m
    #f = np.genfromtxt("/home/auraham/bitbucket/platemo/fronts/f_m_%d.txt" % M)
    
    # c = zeros(size(f,1),M-1);
    # for i = 1 : size(f,1)
    #     for j = 2 : M
    #         temp = f(i,j)/f(i,1)*prod(c(i,M-j+2:M-1));
    #         c(i,M-j+1) = sqrt(1/(1+temp^2));
    #     end
    # end
    rows = f.shape[0]
    cols = M-1
    c = np.zeros((rows, cols))
    for i in range(rows):     # index i
        for j in range(1, M): # index j
            
            
            temp = f[i, j]/f[i,0] * np.prod(c[i, M-j+2-1-1:M-1])
            c[i,M-j-1] = np.sqrt(1/(1+temp**2))
            
            
            a = M-j+2-1-1
            b = M-1
            #print("i: %d, a: %d, b: %d" % (i, a, b))
            
            a = f[i, j]/f[i,0]
            b = np.prod(c[i, M-j+2-1-1:M-1])
            #print("i: %d, a: %.4f, b: %.4f" % (i, a, b))
            
    
    #print_matrix(c)
    
    # if M > 5
    #     c = c.*(cos(pi/8)-cos(3*pi/8)) + cos(3*pi/8);
    # else
    #     c(any(c<cos(3*pi/8)|c>cos(pi/8),2),:) = [];
    # end
    
    #print("points: %d" % c.shape[0])
    
    if M > 5:
        c = c * (np.cos(np.pi/8) - np.cos(3*np.pi/8)) + np.cos(3*np.pi/8)
    else:
        outliers = (c < np.cos(3*np.pi/8)) | (c > np.cos(np.pi/8))
        to_remove = np.any(outliers, axis=by_row)
        c = c[~to_remove].copy()
    
    #print("points: %d" % c.shape[0])
    
    
    # f = fliplr(cumprod([ones(size(c,1),1),c(:,1:M-1)],2)) .* [ones(size(c,1),1), sqrt(1-c(:,M-1:-1:1).^2)];
    #     B      A                                              C                  D
    
    n = c.shape[0]
    col = np.ones((n, 1))
    merge = np.hstack((
                col, 
                c[:, :M-1]
                ))
    A = np.cumprod(merge, axis=by_row)  # cumprod([ones(size(c,1),1),c(:,1:M-1)],2)) 
    B = np.fliplr(A)
    
    rc = np.fliplr(c)                   # reversed c
    merge = np.hstack((
                col,
                np.sqrt(1 - rc**2)      # sqrt(1-c(:,M-1:-1:1).^2)
                ))
                
    f = B * merge
    
    return f.copy()
    
    
if __name__ == "__main__":
    
    import os, sys

    # add lab's path to sys.path
    script_path = os.path.dirname(os.path.abspath(__file__))            # eg: /home/auraham/bitbucket/labs/improved_mss/experiments/test_amoead_mss_v4_case_b
    lab_path = os.path.dirname(os.path.dirname(script_path))            # eg: /home/auraham/bitbucket/labs/improved_mss
    sys.path.insert(0, lab_path)

    from rocket.plot import load_rcparams, plot_pops, parallel_coordinates
    from rocket.fronts import get_real_front
    np.seterr(all='raise')
    
    m_objs = 10
    front = get_front_maf2(m_objs)
    pops = (front, )
    labels = ("maf2", )
    
    # plot
    load_rcparams((12, 10))
    
    if m_objs == 3:
        plot_pops(pops, labels)
    else:
        parallel_coordinates(pops, labels)
        
    # just for comparison
    #np.savetxt("front_maf2_m_%d.txt" % m_objs, front, fmt="%.30f")
