# maf3.py
from __future__ import print_function
from pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np
by_row = 1

def get_front_maf3(m_objs, h):
    """
    Pareto front of MaF3
    """
    
    """
    matlab
    f    = UniformPoint(input,Global.M).^2;
    temp = sum(sqrt(f(:,1:end-1)),2) + f(:,end);
    f    = f./[repmat(temp.^2,1,size(f,2)-1),temp];
    """
    
    # uniform set of vectors
    f = simplex_lattice(m_objs, h) ** 2                 # (pop_size, m_objs)
    
    # shape
    pop_size, _ = f.shape
    
    # temp = sum(  sqrt(f(:,1:end-1))  ,2) + f(:,end);
    #                   A                    B 
    A = f[:, :m_objs-1]                                 # (pop_size, m_objs-1)
    B = f[:, m_objs-1]                                  # (pop_size, )
    temp = np.sum(np.sqrt(A), axis=by_row) + B          # (pop_size, )
    temp = temp.reshape((pop_size, 1))                  # (pop_size, 1)
    
    
    # f    = f./[repmat(temp.^2,1,size(f,2)-1),temp];
    #            A      B         m_objs-1
    
    B = temp**2                                         # (pop_size, 1)
    A = np.tile(B, [1, m_objs-1])                       # (pop_size, m_objs-1)
    C = np.hstack((A, temp))                            # (pop_size, m_objs)
    
    f = f / C
    
    return f.copy()
