# create_fronts.py
from __future__ import print_function
from pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np
from numpy.linalg import norm 
import os, sys
np.seterr(all='raise')

from pareto_fronts.maf1 import get_front_maf1
from pareto_fronts.maf2 import get_front_maf2
from pareto_fronts.maf3 import get_front_maf3
from pareto_fronts.maf4 import get_front_maf4
from pareto_fronts.maf5 import get_front_maf5
from pareto_fronts.maf6 import get_front_maf6
from pareto_fronts.maf13 import get_front_maf13

        
def create_linear_front(m_objs, h, scale=None):
    """
    Create the linear Pareto front of DTLZ1 according to 
    [Li15] An Evolutionary Many-Objective Optimization Algorithm Based on Dominance and Decomposition
    
    Input
    m_objs      int, number of objectives
    scale       (m_objs, ) scale[i] is the scaling factor for the  i-th objective (ie fronts[i, :]). 
                If scale is None, the objectives are not scaled.
                
    Output
    front       (n_points, m_objs) sample front
    """

    # uniform set of vectors
    vectors = simplex_lattice(m_objs, h)
    
    n_points = vectors.shape[0]
    
    by_row = 1
    div = vectors.sum(axis=by_row).reshape((n_points, 1))
    
    # linear pareto front
    front = 0.5 * (vectors / div)            # Eq (14) [Li15] 
    
    # scale each dimension (optional)
    if scale is not None:
        for m in range(m_objs):
            front[:, m] = front[:, m] * scale[m]
    
    return front.copy()
    
    
def create_spherical_front(m_objs, h, scale=None):
    """
    Create the spherical Pareto front of DTLZ2-4 according to 
    [Li15] An Evolutionary Many-Objective Optimization Algorithm Based on Dominance and Decomposition
    
    Input
    m_objs      int, number of objectives
    scale       (m_objs, ) scale[i] is the scaling factor for the  i-th objective (ie fronts[i, :]). 
                If scale is None, the objectives are not scaled.
    
    Output
    front       (n_points, m_objs) sample front
    """
    
    # uniform set of vectors
    vectors = simplex_lattice(m_objs, h)
    
    n_points = vectors.shape[0]
    
    by_row = 1
    div = norm(vectors, axis=by_row).reshape((n_points, 1))
    
    # spherical pareto front
    front = vectors / div                   # Eq (17) [Li15] 
    
    # scale each dimension (optional)
    if scale is not None:
        for m in range(m_objs):
            front[:, m] = front[:, m] * scale[m]
    
    return front.copy()
    

def create_inverted_linear_front(m_objs, h, scale=None):
    """
    Create the inverted linear Pareto front of inv-DTLZ1 according to 
    [Tian17] PlatEMO: A MATLAB Platform for Evolutionary Multi-Objective Optimization
    
    Input
    m_objs      int, number of objectives
    scale       (m_objs, ) scale[i] is the scaling factor for the  i-th objective (ie fronts[i, :]). 
                If scale is None, the objectives are not scaled.
                
    Output
    front       (n_points, m_objs) sample front
    """

    # uniform set of vectors
    vectors = simplex_lattice(m_objs, h)
    
    # inverted front as defined in IDTLZ1.m
    front = (1 - vectors) / 2
    
    # scale each dimension (optional)
    if scale is not None:
        for m in range(m_objs):
            front[:, m] = front[:, m] * scale[m]
    
    return front.copy()
    
    
def get_front(mop_name, m_objs, h):
    
    if mop_name == "dtlz1":
        return create_linear_front(m_objs, h)

    elif mop_name == "inv-dtlz1":
        return create_inverted_linear_front(m_objs, h)

    elif mop_name in ("dtlz2", "dtlz3"):
        return create_spherical_front(m_objs, h)
        
    elif mop_name == "maf1":
        return get_front_maf1(m_objs, h)
        
    elif mop_name == "maf2":
        return get_front_maf2(m_objs, h)
        
    elif mop_name == "maf3":
        return get_front_maf3(m_objs, h)
        
    elif mop_name == "maf4":
        return get_front_maf4(m_objs, h)
        
    elif mop_name == "maf5":
        return get_front_maf5(m_objs, h)
        
    elif mop_name == "maf6":
        return get_front_maf6(m_objs, h)
        
    elif mop_name == "maf13":
        return get_front_maf13(m_objs, h)
        
    else:
        print("Problem not found (mop_name: %s)" % mop_name)
    
    
if __name__ == "__main__":
    
    from rocket.plot import load_rcparams, plot_pops, parallel_coordinates
    
    # --- test ----
    
    front_lin = create_linear_front(m_objs=3)
    front_sph = create_spherical_front(m_objs=3)
    front_inv = create_inverted_linear_front(m_objs=3)
    
    # plot
    load_rcparams((12, 10))
    
    pops = (front_lin, front_sph, front_inv)
    labels = ("linear front", "spherical front", "inverted front")
    #plot_pops(pops, labels)
    
    # --- maf ---
    m_objs = 10
    maf1 = get_front("maf1", m_objs)
    maf2 = get_front("maf2", m_objs)
    maf3 = get_front("maf3", m_objs)
    maf4 = get_front("maf4", m_objs)
    maf5 = get_front("maf5", m_objs)
    maf6 = get_front("maf6", m_objs)
    maf13 = get_front("maf13", m_objs)
    
    pops = (maf1, maf2, maf3, maf4, maf5, maf6, maf13)
    labels = ("maf1", "maf2", "maf3", "maf4", "maf5", "maf6", "maf13")
    parallel_coordinates(pops, labels)
    
    # -- print extreme solutions (z_ideal, z_nadir) --
    
    dtlz1 = get_front("dtlz1", m_objs)
    dtlz2 = get_front("dtlz2", m_objs)
    dtlz3 = get_front("dtlz3", m_objs)
    inv_dtlz1 = get_front("inv-dtlz1", m_objs)
    
    pops = (dtlz1, dtlz2, dtlz3, inv_dtlz1, maf1, maf3, maf4, maf5, maf6, maf13)
    labels = ("dtlz1", "dtlz2", "dtlz3", "inv-dtlz1", "maf1", "maf3", "maf4", "maf5", "maf6", "maf13")
    
    for objs, lbl in zip(pops, labels):
        
        z_min = objs.min(axis=0)
        z_max = objs.max(axis=0)
        
        print(lbl)
        print("z_min", z_min)
        print("z_max", z_max)
        print("")
        
    
    # --- now for m=3 objs ---
    # according to these plots:
    # maf1: linear, inverted
    # maf2: concave (not-complete sphere)
    # maf3: convex, triangular
    # maf4: convex, badly-scaled, inverted
    # maf5: concave, badly-scaled (think of it as a badly-scaled dtlz3)
    # maf6: degenerate
    # maf13: concave
    
    # thus, I think that the paper 
    # [Cheng17] A benchmark test suite for evolutionary many-objective optimization
    # has these typos in Table 1
    # 
    # Problem
    # MaF4: concave, multimodal   -> should be -> replace "concave" with "convex"
    # MaF5: convex, biased        -> should be -> replace "convex"  with "concave"
    m_objs = 3
    maf1 = get_front("maf1", m_objs)
    maf2 = get_front("maf2", m_objs)
    maf3 = get_front("maf3", m_objs)
    maf4 = get_front("maf4", m_objs)
    maf5 = get_front("maf5", m_objs)
    maf6 = get_front("maf6", m_objs)
    maf13 = get_front("maf13", m_objs)
    
    pops = (maf1, maf2, maf3, maf6, maf13)
    labels = ("maf1", "maf2", "maf3", "maf6", "maf13")
    plot_pops(pops, labels)
    
    # badly-scaled
    pops = (maf4, )
    labels = ("maf4", )
    plot_pops(pops, labels)
    
    # badly-scaled
    pops = (maf5, )
    labels = ("maf5", )
    plot_pops(pops, labels)
    
    
    
    
    
    
    
    
