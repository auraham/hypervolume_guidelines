# maf6.py
from __future__ import print_function
from pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np
by_col = 0
by_row = 1

def get_front_maf6(m_objs, h):
    """
    Pareto front of MaF6
    """
    
    """
    matlab
    f = UniformPoint(input,I);
    f = f./repmat(sqrt(sum(f.^2,2)),1,size(f,2));
    f = [f(:,ones(1,Global.M-size(f,2))),f];
    f = f./sqrt(2).^repmat(max([Global.M-I,Global.M-I:-1:2-I],0),size(f,1),1);
    """
    
    # uniform set of vectors
    i_objs = 2
    f = simplex_lattice(i_objs, h)                      # (pop_size, i_objs)
    
    # shape
    pop_size, _ = f.shape
    
    
    # f = f./repmat(sqrt(sum(f.^2,2)),1,size(f,2));
    #        A      B                   i_objs
    
    
    B = np.sqrt(np.sum(f**2, axis=by_row))              # (pop_size, )
    B = B.reshape((pop_size, 1))                        # (pop_size, 1)
    A = np.tile(B, [1, i_objs])                         # (pop_size, i_objs)
    f = f / A
    
    
    # f = [ f(:,ones(1,Global.M-size(f,2))),f];
    #     B A   col                          
    
    rep = m_objs - i_objs                               # copy first column of f rep times
    col = f[:, 0].reshape((pop_size, 1))                # first column of f
    A   = np.tile(col, [1, rep])                        # (pop_size, m_objs-i_objs) copy col rep times to the right
    B   = np.hstack((A, f))                             # stack (pop_size, m_objs-i_objs) and (pop_size, i_objs) 
    f   = B.copy() 
    
    # f = f./sqrt(2).^repmat(max([Global.M-I,Global.M-I:-1:2-I],0),size(f,1),1);
    #        B        A      n                                     pop_size
    
    
    start = m_objs-i_objs   # inclusive
    stop = 2-i_objs-1       # exclusive
    step = -1
    p = [m_objs-i_objs] + [ i for i in range(start, stop, step) ]
    p = np.array(p)                                                     # powers, for m_objs=8, i_objs=2, p = (6, 6, 5, 4, 3, 2, 1, 0)
    
    lp = len(p)
    p = p.reshape((1, lp))                                              # (1, m_objs) reshape to row
    
    b = np.zeros(p.shape)                                               # vector of zeros
    n = np.max(np.vstack((p, b)), axis=by_col)                          # (1, m_objs) max element per row
    
    A = np.tile(n, [pop_size, 1])                                       # (pop_size, m_objs)
    B = np.power(np.sqrt(2), A)                                         # (pop_size, m_objs) THIS LINE COULD BE OPTIMIZED 
                                                                        # something like 
                                                                        # B = np.power(2, n) [to compute the powers of a single row ];
                                                                        # B = np.tile(B.reshape((1, m_objs)), [pop_size, 1])    to repeat the results to the bottom
                                                                        # this way, you compute powers m_objs times instead of pop_size*m_objs 
    f = f / B
    
    return f.copy()
