# maf5.py
from __future__ import print_function
from pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np
by_row = 1

def get_front_maf5(m_objs, h):
    """
    Pareto front of MaF5
    """
    
    """
    matlab
    f = UniformPoint(input,Global.M);
    f = f./repmat(sqrt(sum(f.^2,2)),1,Global.M);
    f = f.*repmat(2.^(Global.M:-1:1),size(f,1),1);
    """
    
    # uniform set of vectors
    f = simplex_lattice(m_objs, h)                      # (pop_size, m_objs)
    
    # shape
    pop_size, _ = f.shape
    
    
    # f = f./repmat(sqrt(sum(f.^2,2)),1,Global.M);
    #        A      B
    
    
    B = np.sqrt(np.sum(f**2, axis=by_row))              # (pop_size, )
    B = B.reshape((pop_size, 1))                        # (pop_size, 1)
    A = np.tile(B, [1, m_objs])                         # (pop_size, m_objs)
    f = f / A
    
    # f = f.*repmat(2.^(Global.M:-1:1),size(f,1),1);
    #        A      B
    
    start = m_objs
    stop = 0
    step = -1
    B = np.array([[2**p for p in range(start, stop, step)]])        # 2**m_objs, 2**{m_objs-1}, ..., 2**1
    A = np.tile(B, [pop_size, 1])                                   # (pop_size, m_objs)
    f = f * A                                                       # (pop_size, m_objs)
    
    return f.copy()
