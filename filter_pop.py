# filter_pop.py
from __future__ import print_function
import numpy as np
import os, sys, argparse

from par_vfns import par_filter_pop
from normalize import normalize

# replace this block
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")
from rocket.pareto_fronts import get_front
from rocket.plot import load_rcparams, plot_pops, parallel_coordinates


def save_filtered_objs(filtered_objs, filtered_filepath, add_separators=True):
    
    if filtered_objs.shape[0] > 0:
        
        # save filtered pop
        #np.savetxt(output_filepath, objs_b, header=" ", footer=" ", comments="#")

        with open(filtered_filepath, "w") as log:
        
        
            if add_separators:
                log.write("#\n")
        
            for point in filtered_objs:
                
                line = " ".join([ "%.18e" % val for val in point ])
                log.write(line+"\n")
                
            if add_separators:
                log.write("#\n")
            
    else:
        
        with open(filtered_filepath, "w") as log:
            
            log.write("#\n#\n")
            


def filter_by_reference_point(pop, ref_point):
    """
    Keep the points in pop inferior than ref_point
    """
    
    pop_size, m_objs = pop.shape
    to_keep = np.zeros((pop_size, ), dtype=bool)
    
    for i, u in enumerate(pop):
        
        if (u < ref_point).all():        # estrictamente menor
            to_keep[i] = True
            
    return pop[to_keep].copy()


def str_vector(vect):
    return " ".join(["%.4f" % v for v in vect ])

if __name__ == "__main__":
    
    
    # select example
    run_example_dtlz3 = False
    run_example_maf4 = True
    
    # --- scaled problem ---
    
    if run_example_dtlz3:
        
        # load input file
        input_filepath  = "input/objs_moead-m-8-fev-te-dtlz3-h-7_m_8_run_0_final.txt"
        output_filepath = "output/pop.txt"
                    
        # load objs
        objs = np.genfromtxt(input_filepath)
        
        # size
        pop_size, m_objs = objs.shape
        
        # ref point
        ref_point = np.array([1.1] * m_objs)

        # filter 1: reference point only
        objs_a = filter_by_reference_point(objs, ref_point)
        save_filtered_objs(objs_a, output_filepath + ".filter_ref_point")
        print("filter 1: completed")
        
        # filter 2: nds only (first front)
        objs_b = par_filter_pop(objs, procs=12)
        save_filtered_objs(objs_b, output_filepath + ".filter_nds")
        print("filter 2: completed")
        
        # filter 3: ref point + nds (first front)
        objs_c = filter_by_reference_point(objs, ref_point)
        objs_d = par_filter_pop(objs_c, procs=12)
        save_filtered_objs(objs_d, output_filepath + ".filter_both")
        print("filter 3: completed")
        
        # filter 4: normalization + ref point + nds (first front)
        z_ideal = np.zeros((m_objs, ))
        z_nadir = np.ones((m_objs, ))
        objs_n = normalize(objs, z_ideal, z_nadir)
        objs_c = filter_by_reference_point(objs_n, ref_point)
        objs_d = par_filter_pop(objs_c, procs=12)
        save_filtered_objs(objs_d, output_filepath + ".filter_norm_both")
        print("filter 4: completed")
        
    
    # --- badly scaled problem ---
    
    if run_example_maf4:
        
        # load input file
        input_filepath  = "input/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_1_t_500_fev_ws.txt"
        output_filepath = "output/pop_mss.txt"
        output_front_filepath = "output/front_mss.txt"
        
        # load objs
        objs = np.genfromtxt(input_filepath)
        
        # size
        pop_size, m_objs = objs.shape
        
        # load real front
        mop_name = "maf4"
        front = get_front(mop_name, m_objs)
        
        # define z_ideal and z_nadir
        z_ideal = front.min(axis=0)
        z_nadir = front.max(axis=0)
        
        print("z_ideal:", str_vector(z_ideal))
        print("z_nadir:", str_vector(z_nadir))
        
        # ref point
        # use [1.1, 1.1, ..., 1.1] if the population 
        # is normalized in the range [0, 1]
        ref_point = np.array([1.1] * m_objs)
        
        # --- debug ---
        pops = (front, objs)
        lbls = ("front", "objs")
        plot_pops(pops, lbls, output="img/fig_maf4_before_normalization.png")
        
        # filter: normalization + ref point + nds (first front)
        front_n = normalize(front, z_ideal, z_nadir)
        objs_n = normalize(objs, z_ideal, z_nadir)
        objs_c = filter_by_reference_point(objs_n, ref_point)
        objs_d = par_filter_pop(objs_c, procs=4)
        save_filtered_objs(objs_d, output_filepath + ".filter_norm_both")           # save pop
        save_filtered_objs(front_n, output_front_filepath + ".filter_norm_both")    # save front
        print("filter: completed")
        
        # --- debug ---
        pops = (front_n, objs_n)
        lbls = ("front norm", "objs norm")
        plot_pops(pops, lbls, output="img/fig_maf4_after_normalization.png")
        
