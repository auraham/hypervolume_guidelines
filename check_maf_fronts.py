# check_maf_fronts.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
np.seterr(all='raise')

# add rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.pareto_fronts import get_front

def str_vector(vec):
    return " ".join(["%.4f" % v for v in vec])

if __name__ == "__main__":
    
    mop_names = (
                "dtlz1", "dtlz2", "dtlz3", "inv-dtlz1",
                "maf1", "maf2", "maf3", "maf4", "maf5",
                )
    
    for mop_name in mop_names:
        
        print("-- %s --" % mop_name) 
        
        for m_objs in (3, 5, 8, 10):
            
            front = get_front(mop_name, m_objs)
            z_nadir = front.max(axis=0)
            
            print("m_objs: %2d, size: %3d, z_nadir: %s" % (m_objs, front.shape[0], str_vector(z_nadir)))
            
        print("")
    
