# Specification of the Nadir point

This post explains the importance of the Nadir point for hypervolume calculation.



## Introduction

Before computing the hypervolume, we need to normalize the input (either a Pareto front or a solution set) in the range [0, 1]. To this end, we need the Nadir point, that comprise the maximum value for each objective function. If that point is not defined properly, the normalization will be inaccurate, affecting the outcome of the hypervolume.

Run `check_maf_fronts.m` to display the Nadir point of a few test problems:

```
>> check_maf_fronts
```

```
-- dtlz1 --
m_objs=3, 0.5000 0.5000 0.5000 
m_objs=5, 0.5000 0.5000 0.5000 0.5000 0.5000 
m_objs=8, 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 
m_objs=10, 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 
-- dtlz2 --
m_objs=3, 1.0000 1.0000 1.0000 
m_objs=5, 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=8, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=10, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
-- dtlz3 --
m_objs=3, 1.0000 1.0000 1.0000 
m_objs=5, 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=8, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=10, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
-- inv-dtlz1 --
m_objs=3, 0.5000 0.5000 0.5000 
m_objs=5, 0.5000 0.5000 0.5000 0.5000 0.5000 
m_objs=8, 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 
m_objs=10, 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 
-- maf1 --
m_objs=3, 1.0000 1.0000 1.0000 
m_objs=5, 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=8, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=10, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
-- maf2 --
m_objs=3, 0.8165 0.8165 0.8890 
m_objs=5, 0.1907 0.1907 0.1907 0.6674 0.6674 
m_objs=8, 0.5745 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239 
m_objs=10, 0.4904 0.4904 0.5308 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239 
-- maf3 --
m_objs=3, 1.0000 1.0000 1.0000 
m_objs=5, 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=8, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
m_objs=10, 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 
-- maf4 --
m_objs=3, 2.0000 4.0000 8.0000 
m_objs=5, 2.0000 4.0000 8.0000 16.0000 32.0000 
m_objs=8, 2.0000 4.0000 8.0000 16.0000 32.0000 63.9999 127.9999 255.9997 
m_objs=10, 2.0000 4.0000 8.0000 16.0000 32.0000 63.9999 127.9999 255.9997 511.9995 1023.9990 
-- maf5 --
m_objs=3, 8.0000 4.0000 2.0000 
m_objs=5, 32.0000 16.0000 8.0000 4.0000 2.0000 
m_objs=8, 256.0000 128.0000 64.0000 32.0000 16.0000 8.0000 4.0000 2.0000 
m_objs=10, 1024.0000 512.0000 256.0000 128.0000 64.0000 32.0000 16.0000 8.0000 4.0000 2.0000 
```

Now, run `check_maf_front.py` as follows:

```
python3 check_maf_fronts.py 
```

```
-- dtlz1 --
m_objs:  3, size:  91, z_nadir: 0.5000 0.5000 0.5000
m_objs:  5, size: 210, z_nadir: 0.5000 0.5000 0.5000 0.5000 0.5000
m_objs:  8, size: 120, z_nadir: 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000
m_objs: 10, size: 220, z_nadir: 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000

-- dtlz2 --
m_objs:  3, size:  91, z_nadir: 1.0000 1.0000 1.0000
m_objs:  5, size: 210, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs:  8, size: 120, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs: 10, size: 220, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000

-- dtlz3 --
m_objs:  3, size:  91, z_nadir: 1.0000 1.0000 1.0000
m_objs:  5, size: 210, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs:  8, size: 120, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs: 10, size: 220, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000

-- inv-dtlz1 --
m_objs:  3, size:  91, z_nadir: 0.5000 0.5000 0.5000
m_objs:  5, size: 210, z_nadir: 0.5000 0.5000 0.5000 0.5000 0.5000
m_objs:  8, size: 120, z_nadir: 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000
m_objs: 10, size: 220, z_nadir: 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000 0.5000

-- maf1 --
m_objs:  3, size:  91, z_nadir: 1.0000 1.0000 1.0000
m_objs:  5, size: 210, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs:  8, size: 120, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs: 10, size: 220, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000

-- maf2 --
m_objs:  3, size: 130, z_nadir: 0.8308 0.8308 0.9185
m_objs:  5, size:  85, z_nadir: 0.5835 0.5835 0.7001 0.7947 0.9117
m_objs:  8, size: 120, z_nadir: 0.5745 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239
m_objs: 10, size: 220, z_nadir: 0.4904 0.4904 0.5308 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239

-- maf3 --
m_objs:  3, size:  91, z_nadir: 1.0000 1.0000 1.0000
m_objs:  5, size: 210, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs:  8, size: 120, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000
m_objs: 10, size: 220, z_nadir: 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000

-- maf4 --
m_objs:  3, size:  91, z_nadir: 2.0000 4.0000 8.0000
m_objs:  5, size: 210, z_nadir: 2.0000 4.0000 8.0000 16.0000 32.0000
m_objs:  8, size: 120, z_nadir: 2.0000 4.0000 8.0000 16.0000 32.0000 64.0000 128.0000 256.0000
m_objs: 10, size: 220, z_nadir: 2.0000 4.0000 8.0000 16.0000 32.0000 64.0000 128.0000 256.0000 512.0000 1024.0000

-- maf5 --
m_objs:  3, size:  91, z_nadir: 8.0000 4.0000 2.0000
m_objs:  5, size: 210, z_nadir: 32.0000 16.0000 8.0000 4.0000 2.0000
m_objs:  8, size: 120, z_nadir: 256.0000 128.0000 64.0000 32.0000 16.0000 8.0000 4.0000 2.0000
m_objs: 10, size: 220, z_nadir: 1024.0000 512.0000 256.0000 128.0000 64.0000 32.0000 16.0000 8.0000 4.0000 2.0000
```

As it can be seen, both scripts differ on the Nadir point of MaF2:

**matlab**

```
-- maf2 --
m_objs=3, 0.8165 0.8165 0.8890 
m_objs=5, 0.1907 0.1907 0.1907 0.6674 0.6674 
m_objs=8, 0.5745 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239 
m_objs=10, 0.4904 0.4904 0.5308 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239 
```

**python**

```
 -- maf2 --
m_objs: 3, z_nadir: 0.8308 0.8308 0.9185
m_objs: 5, z_nadir: 0.5835 0.5835 0.7001 0.7947 0.9117
m_objs: 8, z_nadir: 0.5745 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239
m_objs: 10, z_nadir: 0.4904 0.4904 0.5308 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239
```









**lo que falta del post**

- [x] actualizar la salida de los comandos de matlab
- [ ] incluir el popsize en el comando de matlab para indicar que el front de maf2 m=5 solo tiene un punto
- [ ] describir la modificacion que hicismos en `get_front` y`maf2.py` para evitar este issue (al incrementar el numero de puntos del frente):

```python
# maf2
def simplex_lattice_maf2(m_objs):

# spacing H1, H2
    spacing = {
        2: (99, 0),
        3: (37, 0),         # 130 puntos
        5: (15, 0),         #  86 puntos
        8: (3, 0),          # 120 puntos
        10: (3, 0),         # 220 puntos
    }
```



- [x] podriamos volver a ejecutar `create_pareto_front.m` y aumentar H par igualar el de `maf2.py`, luego, compararemos la salida con la de python, es decir, esta:

```
(dev) auraham@rocket:~/git/hypervolume_guidelines$ python check_maf_fronts.py 


-- maf2 --
m_objs:  3, size: 130, z_nadir: 0.8308 0.8308 0.9185
m_objs:  5, size:  85, z_nadir: 0.5835 0.5835 0.7001 0.7947 0.9117
m_objs:  8, size: 120, z_nadir: 0.5745 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239
m_objs: 10, size: 220, z_nadir: 0.4904 0.4904 0.5308 0.5745 0.6219 0.6731 0.7286 0.7886 0.8536 0.9239

```







- [ ] obtener el `pop_size` en funcion de `H` y `m_objs` para cada instancia de `maf2`
- [ ] usar el script `create_pareto_front.m` para guardar los fronts de `maf2`
- [ ] modificar `get_front` para devolver esos frentes

