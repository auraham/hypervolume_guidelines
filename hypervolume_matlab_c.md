# Comparison of implementations

This post compares two implementations for computing the hypervolume indicator:

- [wfg]
- [platemo]



## Notes about implementation in Platemo



```matlab
% HV.m
function Score = HV(PopObj,PF,RefPoint)

	[N,M]    = size(PopObj);
	
	% Instead of using [1.1, 1.1, ..., 1.1]
	% we use [1+1/H, 1+1/H, ..., 1+1/H]
	%RefPoint = max(PF,[],1)*1.1;                       
                                                        
	# This is the filtering process
	# It seems to remove those solutions worsen than 
	# the reference point only, that is, 
	# dominated solutions are not filtered in this implementation
	PopObj(any(PopObj>repmat(RefPoint,N,1),2),:) = [];      

```

Es importante que los archivos de entrada (ie `PopObj`) sean filtrados antes de usar `wfg` y `HV.m` para que la salida sea lo mas similar posible.





```matlab
function Score = NHV(PopObj,PF)

    [N,M]    = size(PopObj);
    
    # HV.m and NHV.m differ on this line
    # (I think that this may be not the best way to normalize the HV;
    # I prefer this way: NHV(P) = HV(P) / HV(Pareto_front) ) 
    PopObj   = PopObj./repmat(max(PF,[],1)*1.1,N,1);
    PopObj(any(PopObj>1,2),:) = [];
    
    
    # HV.m and NHV.m differ on this line
    RefPoint = ones(1,M);
```

Te recomiendo que hagas un `diff` de `HV` y `NHV` para que resaltes las dos diferencias de arriba. En este post, usaremos `HV.m` para normalizar el hv de este modo:

```
NHV(P) = HV(P) / HV(Pareto_front) 
```

De este modo, podemos comparar la salida de `wfg` con la de `HV.m` para determinar si la normalizacion es similar en ambas implementaciones.





## Comparison

In the following, we compare the output of both `run_hv_seq.py` and `run_hv_seq.m`. The input populations are stored in `input`.

----

**Note** These results were obtained in `rev 8884b65`.

----

Let's test `run_hv_seq.py` as follows:

```
# debug: use pops in input 
# %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 5 -t 300 -e input
```

Output:

```
In [1]: %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 5 -t 300 -e input                            
task_1[10885] INFO     Starting task (pid 1)
task_1[10885] INFO     moea_name:           mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
task_1[10885] INFO     mop_name:            maf1
task_1[10885] INFO     m_objs:              3
task_1[10885] INFO     runs:                5
task_1[10885] INFO     iters:               300
task_1[10885] INFO     z_ideal:             [0. 0. 0.]
task_1[10885] INFO     z_nadir:             [1. 1. 1.]
task_1[10885] INFO     ref_point:           [1.1, 1.1, 1.1]
task_1[10885] INFO     experiment_path:     input
task_1[10885] INFO     experiment_path_git: None
task_1[10885] INFO     hv_raw: 0.2713278487 hv_norm: 0.2038526287
task_1[10885] INFO     hv_raw: 0.2713354735 hv_norm: 0.2038583572
task_1[10885] INFO     hv_raw: 0.2630277848 hv_norm: 0.1976166677
task_1[10885] INFO     hv_raw: 0.2708046655 hv_norm: 0.2034595534
task_1[10885] INFO     hv_raw: 0.2713669379 hv_norm: 0.2038819970
```

Now, use `run_hv_seq.m`:

```
>> run_hv_seq.m
```

Output:

```
0.2713278487
0.2713354735
0.2630277848
0.2708046655
0.2713669379
```

As it can be seen, the column `hv_raw` is the same in both scripts.

