# plot_pops.py
from __future__ import print_function
import numpy as np
import os, sys, argparse

# add rocket into sys.path
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.plot import load_rcparams, plot_pops, parallel_coordinates

if __name__ == "__main__":
    
    load_rcparams((12, 4))

    objs_a = np.genfromtxt("output/pop.txt.filter_ref_point")
    objs_b = np.genfromtxt("output/pop.txt.filter_nds")
    objs_c = np.genfromtxt("output/pop.txt.filter_both")
    
    
    # --- all pops ---
    pops = (objs_a, objs_b, objs_c)
    lbls = ("Filter ref point", "Filter nds", "Filter ref point + nds")
    
    title = "Pops"
    output = "img/all_pops.png"
    parallel_coordinates(pops, lbls, title=title, output=output)
    

    # --- just pop b ---
    pops = (objs_b,)
    lbls = ("Filter nds", )
    
    title = "Pops"
    output = "img/pop_b.png"
    parallel_coordinates(pops, lbls, title=title, output=output)
    
    
    # --- pop a and pop c ---
    pops = (objs_a, objs_c)
    lbls = ("Filter ref point", "Filter ref point + nds")
    
    title = "Pops"
    output = "img/pop_a_c.png"
    parallel_coordinates(pops, lbls, title=title, output=output)
    
    # --- pop c and pop a ---
    pops = (objs_c, objs_a)
    lbls = ("Filter ref point + nds", "Filter ref point")
    
    title = "Pops"
    output = "img/pop_c_a.png"
    parallel_coordinates(pops, lbls, title=title, output=output)
    
