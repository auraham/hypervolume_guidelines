# log.py
from __future__ import print_function
import logging
import coloredlogs
from distutils.dir_util import mkpath
import os

coloredlogs.DEFAULT_LOG_FORMAT = '%(name)s[%(process)d] %(levelname)-8s %(message)s'    # console format
coloredlogs.DEFAULT_LOG_FORMAT = '%(name)s %(levelname)-8s %(message)s'    # console format
MSG_FMT = "%(asctime)s - %(name)s - %(levelname)-8s - %(message)s"                      # file format

def get_log(logname):
    """
    Return a logger object to record messages from concurrent tasks
    
    Input
    name        id of the logger
    
    Output
    log         logger
    """
    
    # create log directory
    path = "log"
    mkpath(path)
    
    # filename
    filename = "%s.log" % logname
    filepath = os.path.join(path, filename)
    
    # By default the install() function installs a handler on the root logger,
    # this means that log messages from your code and log messages from the
    # libraries that you use will all show up on the terminal.
    coloredlogs.install(level='DEBUG')
    
    # create logger
    log = logging.getLogger(logname)
    log.setLevel(logging.DEBUG)
    
    # create handler
    ch = logging.StreamHandler()
    fh = logging.FileHandler(filepath)
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
    
    # create formatter
    #ch_form = logging.Formatter(MSG_FMT)
    fh_form = logging.Formatter(MSG_FMT)
    
    # add formatters
    #ch.setFormatter(ch_form)
    fh.setFormatter(fh_form)
    
    # add ch to logger
    # https://stackoverflow.com/questions/7173033/duplicate-log-output-when-using-python-logging-module
    if (log.hasHandlers()):
        log.handlers.clear()
    #log.addHandler(ch)
    log.addHandler(fh)
    
    return log
      
if __name__ == "__main__":
    
    # example
    pid = 2
    config = {'mop_name': 'inv-dtlz1', 'iters': 1000, 'm_objs': 10}
    logger = get_log(config, pid)


    # application code
    logger.debug("debug")
    logger.info("info")
    logger.warning("warning")
    logger.error("error")
    logger.critical("critical")
    
    try:
        0/0
    except Exception as e:
        
        # print traceback
        logger.error("ouch", exc_info=True)
