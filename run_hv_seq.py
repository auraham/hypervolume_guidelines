# run_hv_seq.py
# 
# %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 50 -t 300 -e /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a -b /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
#
# debug: 5 runs only
# %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 5 -t 300 -e /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a -b /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
#
# debug: use pops in input 
# %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 5 -t 300 -e input

"""
expected output
hv_raw: 0.2713278487 hv_norm: 0.2038526287
hv_raw: 0.2713354735 hv_norm: 0.2038583572
hv_raw: 0.2630277848 hv_norm: 0.1976166677
hv_raw: 0.2708046655 hv_norm: 0.2034595534
hv_raw: 0.2713669379 hv_norm: 0.2038819970
"""

from __future__ import print_function
import numpy as np
import os, sys, argparse
from distutils.dir_util import mkpath
from shutil import copy2
import subprocess

# add rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.helpers import save_filtered_objs
from rocket.helpers import filter_by_reference_point
from rocket.helpers import filter_by_nds
from rocket.helpers import normalize
from rocket.pareto_fronts import get_front

from log import get_log

def compute_hv_seq(moea_name, mop_name, m_objs, runs, iters, experiment_path, experiment_path_git):
    """
    Compute the hypervolume indicator from input files
    """
    
    # get log
    name = "%s_%s_%s" % (moea_name, mop_name, m_objs)
    log = get_log(name)
    
    # get pareto front
    front = get_front(mop_name, m_objs)
    
    # define z_ideal, z_nadir
    z_ideal = front.min(axis=0)
    z_nadir = front.max(axis=0)
    
    # define hv reference point
    ref_point = [1.1] * m_objs
    ref_point_str = ["1.1"] * m_objs
        
    # log
    log.info("moea_name:           %s" % moea_name)
    log.info("mop_name:            %s" % mop_name)
    log.info("m_objs:              %d" % m_objs)
    log.info("runs:                %d" % runs)
    log.info("iters:               %d" % iters)
    log.info("z_ideal:             %s" % z_ideal)
    log.info("z_nadir:             %s" % z_nadir)
    log.info("ref_point:           %s" % ref_point)
    log.info("experiment_path:     %s" % experiment_path)
    log.info("experiment_path_git: %s" % experiment_path_git)
    log.info("remember to normalize hv_raw as hv_norm = hv_raw / hv_raw_front")
    
    # create backup?
    create_backup = False
    backup_path = ""
    
    # define backup directory
    if experiment_path_git:
        backup_path = os.path.join(experiment_path_git, mop_name, moea_name, "stats")
        create_backup = True
        
    # create backup directory, if needed
    if not os.path.isdir(backup_path):
        mkpath(backup_path)

    for run_id in range(runs):
        
        # input file: objs
        filename_objs = "objs_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, iters)
        filepath_objs = os.path.join(experiment_path, mop_name, moea_name, "pops", filename_objs)
        
        # input file: normalized + filtering using ref point and nds
        filepath_objs_filtered = filepath_objs + ".norm.filtered"

        # output file
        filename_hv = "hv_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, iters)
        filepath_hv = os.path.join(experiment_path, mop_name, moea_name, "stats", filename_hv)
        
        # output file (backup)
        if create_backup:
            filepath_hv_copy = os.path.join(backup_path, filename_hv)
        
        # check if file exists
        if not os.path.isfile(filepath_objs):
            print("Error: filepath does not exists (filepath: %s)" % filepath_objs)
            continue
        
        # preprocessing input file
        # normalization + ref point + nds (first front)
        objs = np.genfromtxt(filepath_objs)                                     # load objs
        objs_norm = normalize(objs, z_ideal, z_nadir)                           # normalize in range [0, 1]
        objs_norm_fil_a = filter_by_reference_point(objs_norm, ref_point)       # remove points worsen than [1.1, 1.1, ..., 1.1]
        objs_norm_fil_b = filter_by_nds(objs_norm_fil_a, procs=4)               # remove dominated solutions
        
        # get pop_size after and before filtering
        pop_size  = objs.shape[0]
        pop_size_final = objs_norm_fil_b.shape[0]
        
        # save objs
        save_filtered_objs(objs_norm_fil_b, filepath_objs_filtered)
        
        # debug: use this line if you plan to use run_hv_seq.m as well
        #save_filtered_objs(objs_norm_fil_b, filepath_objs_filtered+".matlab", add_separators=False)
        
        # prepare command
        cmd = ["wfg", "-q", filepath_objs_filtered, *ref_point_str, "-o", filepath_hv]
        
        # call wfg
        proc = subprocess.run(cmd, stdout=subprocess.PIPE)

        # get output from wfg
        output = proc.stdout.decode("utf-8")

        hv_raw, hv_norm = output.replace("\n", "").split(" ")
        log.info("run_id: %d, hv_raw: %s hv_norm: %s, pop_size: %d/%d" % (run_id, hv_raw, hv_norm, pop_size_final, pop_size))
        
        # create backup
        if create_backup:
            copy2(filepath_hv, filepath_hv_copy)
            
            
        # debug
        #print(filepath_objs_filtered)
        


if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-a", "--moea_name",            required=True, help="moea name")
    ap.add_argument("-p", "--mop_name",             required=True, help="mop name")
    ap.add_argument("-m", "--m_objs",               required=True, help="m_objs: 3, 5, 8, 10", type=int, choices=(3,5,8,10))
    ap.add_argument("-r", "--runs",                 required=True, help="number of runs", type=int)
    ap.add_argument("-t", "--iters",                required=True, help="number of generations", type=int)
    ap.add_argument("-e", "--experiment_path",      required=True, help="experiment path")
    ap.add_argument("-b", "--experiment_path_git",  required=False, help="backup of results for git")
    args = vars(ap.parse_args())
    
    compute_hv_seq(                             # example
                args["moea_name"],              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
                args["mop_name"],               # maf1
                args["m_objs"],                 # 3
                args["runs"],                   # 50
                args["iters"],                  # 300
                args["experiment_path"],        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                args["experiment_path_git"]     # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
            )

