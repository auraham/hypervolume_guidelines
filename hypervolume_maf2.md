# MaF2

This post explains my approach for computing the hypervolume for this problem.



## Issue

After computing the hypervolume on several solution sets for MaF2, I noticed that the normalized hypervolume is not on the range [0, 1]:

```
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 0, hv normalized: 1.7571788236639905
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 1, hv normalized: 1.762457732835533
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 2, hv normalized: 1.738712613858204
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 3, hv normalized: 1.7186008629084701
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 4, hv normalized: 1.6981866605279112
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 5, hv normalized: 1.7325072725480228
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 6, hv normalized: 1.7016172548534796
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 7, hv normalized: 1.7605988071862502
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 8, hv normalized: 1.763509802549459
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 9, hv normalized: 1.7549639929588585
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 10, hv normalized: 1.748818822862237
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 11, hv normalized: 1.6112393228543618
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 12, hv normalized: 1.7776000595896337
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 13, hv normalized: 1.7433625225058296
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 14, hv normalized: 1.7407648254403214
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 15, hv normalized: 1.735102387919594
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 16, hv normalized: 1.7162043175876123
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 17, hv normalized: 1.7385242747129714
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 18, hv normalized: 1.7522473286135418
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 19, hv normalized: 1.7636729311139587
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 20, hv normalized: 1.7226447613880638
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 21, hv normalized: 1.7617876350514274
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 22, hv normalized: 1.7148971830077302
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 23, hv normalized: 1.7411924037214943
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 24, hv normalized: 1.771397449835678
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 25, hv normalized: 1.7554480573636926
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 26, hv normalized: 1.7283636598409136
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 27, hv normalized: 1.7539266469831782
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 28, hv normalized: 1.7494808373331179
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 29, hv normalized: 1.7780054416544973
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 30, hv normalized: 1.7587985050120225
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 31, hv normalized: 1.7083076807868878
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 32, hv normalized: 1.779674250685756
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 33, hv normalized: 1.7665054439707193
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 34, hv normalized: 1.7416481795098844
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 35, hv normalized: 1.7636972145061038
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 36, hv normalized: 1.7636648199305829
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 37, hv normalized: 1.722532673994701
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 38, hv normalized: 1.7837424214812854
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 39, hv normalized: 1.738015039051108
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 40, hv normalized: 1.7755083645209795
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 41, hv normalized: 1.7591832404199512
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 42, hv normalized: 1.74444301729745
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 43, hv normalized: 1.7749680980310212
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 44, hv normalized: 1.6946263002543815
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 45, hv normalized: 1.7771786836323749
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 46, hv normalized: 1.7733245146116836
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 47, hv normalized: 1.7513898533044967
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 48, hv normalized: 1.7258282525804483
mss-dci_norm_hyp_mean-divs-5-50-mean-20-ps-normal-ss-gra-norm-true_maf2_10 INFO     run_id: 49, hv normalized: 1.7511443074992
```

For instance, the last solution set has a normalized hypervolume of `hv normalized: 1.7511443074992`. On the other hand, the hypervolume of the Pareto front is shown below:

```
gtoscano@alpha:~/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a/maf2$ cat hv_front_maf2_m_10.txt 
12.74817949592995169894 0.71789479374252318511
```

