# Computing the Hypervolume indicator with Python

**tl;dr** *This post explains how to call `wfg` from python for computing the hypervolume indicator*.



## Introduction

In the previous post, we compute the hypervolume indicator as follows:

0. Normalize the input population in the range `[0, 1]` using `z_ideal` and `z_nadir`.
1. Filter the input population using the reference point (i.e., those points that are not in the range `[0, 1.1]` are removed).
2. Filter again using non-dominanted sorting or NDS (i.e., dominated solutions are removed).
3. Compute the hypervolume indicator.

We use the implementation of the Walking Fish Group (WFG) to this end. For instance, we compute the hypervolume of a solution set stored in `pop_mss.txt.filter_norm_both` as follows:

```
wfg pop_mss.txt.filter_norm_both 1.1 1.1 1.1
```

This is the output:

```
verbose flag: 1, arg_index: 1, argc: 5, ind_filename: 1, ind_ref_vect: 2
filename: pop_mss.txt.filter_norm_both
# fronts: 1
# maxm: 91
# maxn: 3
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
0.689151 0.517769
Time: 0.000000 (s)
Total time = 0.000000 (s)
```

In this post, we explain how to invoke `wfg` from python using the `subprocess` module. The aim of this approach is to ease the automation of this task. Rather than using a couple of bash scripts, we will use python.



## References

- [Executing shell commands with python](https://stackabuse.com/executing-shell-commands-with-python/)

- [run - python subprocess example](https://code-examples.net/en/q/15c8c)