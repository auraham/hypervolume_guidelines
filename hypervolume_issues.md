# Issues when computing the hypervolume

**tl;dr** I compute the indicator incorrectly because the normalization and filtering was wrong.



## Introduction

In this post, we explain an issue found when computing the indicator. I found this bit in the log after computing the indicator on a bunch of files:

**MaF2**, `m_objs=3`

```
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_maf2_3 INFO     run_id: 1, hv normalized: 1.0025395036811728
```

The normalized hypervolume should be in the range [0, 1] but is not. It is a little bit larger than one. At first I supposed it was an accuracy error (we save 30 digits, but it may happen). Then, I inspect other test problems for the same issue. It turns out that other problems have the same issue:

**DTLZ2**, `m_objs=8`

```
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     run_id: 0, hv normalized: 1.0003462864169765
```

**MaF2**, `m_objs=5`

```
mss-dci_norm_hyp_mean-divs-20-10-mean-10-ps-normal-ss-gra-norm-true_maf2_5 INFO     run_id: 46, hv normalized: 1.3139246042354786
```

**MaF3**, `m_objs=10`

```
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_maf3_10 INFO     run_id: 22, hv normalized: 1.0000000080777425
```

**MaF5**, `m_objs=10`

```
mss-dci_norm_hyp_mean-divs-8-50-mean-20-ps-normal-ss-gra-norm-true_maf5_10 INFO     run_id: 49, hv normalized: 1.0002388830812876
```

Notice that the normalized hypervolume value for MaF2 was `1.3139246042354786`. That's too large, meaning  that it is not an accuracy issue. In the following, we will discuss the case for DTLZ2, `m_objs=8` only, because the shape of its Pareto front is the most simple among the other problems. In other words, we will determine why the normalized hypervolume is `1.0003462864169765`.



### Hypervolume of DTLZ2

This part of the log shows the hypervolume calcutation of the Pareto front of DTLZ2 for `m_objs=8`:

```
front_dtlz2_8 INFO     mop_name:            dtlz2
front_dtlz2_8 INFO     m_objs:              8
front_dtlz2_8 INFO     z_ideal:             [0. 0. 0. 0. 0. 0. 0. 0.]
front_dtlz2_8 INFO     z_nadir:             [1. 1. 1. 1. 1. 1. 1. 1.]
front_dtlz2_8 INFO     H:                   3
front_dtlz2_8 INFO     ref_point:           [1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333]
front_dtlz2_8 INFO     experiment_path:     /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a
front_dtlz2_8 INFO     experiment_path_git: /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a_git
front_dtlz2_8 INFO     hv_raw: 9.81485116739975005373 hv_norm: 0.98259336123945373309
front_dtlz2_8 INFO     command: wfg -q /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a/dtlz2/front_dtlz2_m_8.txt 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 -o /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a/dtlz2/hv_front_dtlz2_m_8.txt
```

We are using the recommended approach of Ishibuthi et al. [Ishibuchi17, Ishibuchi18] for setting the reference vector as `r=1+1/H`, where `H` is the same parameter employed for MOEA/D to create the weight vectors. In this case, we use `H=3`, meaning that `r = 1.333`.

The (simplified) command for computing the indicator is:

```
command: wfg -q front_dtlz2_m_8.txt 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 -o hv_front_dtlz2_m_8.txt
```

From the log, we can see that the hypervolume of DTLZ2 is almost `9.8148` (ignore the value of `hv_norm`):

```
front_dtlz2_8 INFO     hv_raw: 9.81485116739975005373 hv_norm: 0.98259336123945373309
```

In other words, the hypervolume of any other solution set should be less or equal to `hv_raw`. However, that's not the case. Back to our example, we known that the issue was caused by the solution set corresponding to `run_id=0`:

```
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     run_id: 0, hv normalized: 1.0003462864169765
```

Let's review the log:

```
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     moea_name:           mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     mop_name:            dtlz2
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     m_objs:              8
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     runs:                50
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     iters:               800
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     z_ideal:             [0. 0. 0. 0. 0. 0. 0. 0.]
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     z_nadir:             [1. 1. 1. 1. 1. 1. 1. 1.]
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     H:                   3
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     ref_point:           [1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333, 1.3333333333333333]
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     experiment_path:     /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     experiment_path_git: /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a_git
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     remember to normalize hv_raw as hv_norm = hv_raw / hv_raw_front
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     run_id: 0, hv_raw: 9.81824991704366745182 hv_norm: 0.98293361997386230478, pop_size: 120/120
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     command: wfg -q /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a/dtlz2/mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_m_8_run_0_t_800.txt.norm.filtered 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 -o /home/gtoscano/git/moead_mss_dci_results/experiments/paper_test_mss_sensitivity/results_case_a/dtlz2/mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true/stats/hv_mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_m_8_run_0_t_800.txt
```

The important part is this line:

```
mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_dtlz2_8 INFO     run_id: 0, hv_raw: 9.81824991704366745182 hv_norm: 0.98293361997386230478, pop_size: 120/120
```

As it can be seen, the hypervolume is almost `9.8182`, a bit larger than the hypervolume of the Pareto front, `9.8148`. How can it be possible? In the following, we will reproduce the hypervolume calculation, identify the issue, and fix it.



## Reproducing the issue

Execute the following command to calculate the hypervolume of the Pareto front of DTLZ2 and its corresponding approximation:

```
(dev) auraham@rocket:~/git/hypervolume_guidelines$ python3 run_hv_issue.py 
```

You will see the log as displayed above.

```
front_dtlz2_8 INFO     hv_raw: 9.81485116739975005373 hv_norm: 0.98259336123945373309
mss.._dtlz2_8 INFO     run_id: 0, hv_raw: 9.81824991704366745182 hv_norm: 0.98293361997386230478, 
```

Now that we have reproduced the error, let's determine its reason.



## Determining the reason of the issue

The following command is employed to calculate the hypervolume of the solution set:

```
wfg -q input/dtlz2/mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_m_8_run_0_t_800.txt.norm.filtered 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 1.3333333333 -o input/dtlz2/mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true/stats/hv_mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_m_8_run_0_t_800.txt
```

Let's check the input file:

```
input/dtlz2/mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true_m_8_run_0_t_800.txt.norm.filtered
```

From `ipython`:

```python
In [1]: filepath = "input/dtlz2/mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-normal-ss-gra-norm-true/pops/objs_mss-dci_norm_hyp_mean-divs-10-10-mean-10-ps-norm
   ...: al-ss-gra-norm-true_m_8_run_0_t_800.txt.norm.filtered"                                       
In [2]: objs = np.genfromtxt(filepath)                                                               
In [3]: objs.max(axis=0)                                                                                                                                      
Out[3]: 
array([1.00017492, 1.00018629, 1.00034925, 1.0001652 , 1.00039271,
       1.00010254, 1.00017487, 1.00007262])
```

As it can be seen, the maximum values of the input file are slightly larger than 1. Remember, this file is supposed to be normalized and filtered (see the `compute_hv_seq()` function):

```python
def compute_hv_seq(...):	
    # preprocessing input file
    # normalization + ref point + nds (first front)
    objs = np.genfromtxt(filepath_objs)                              # load objs
    objs_norm = normalize(objs, z_ideal, z_nadir)                    # normalize in range [0, 1]
    objs_norm_fil_a = filter_by_reference_point(objs_norm, ref_point)# remove points worsen than ref_point
    objs_norm_fil_b = filter_by_nds(objs_norm_fil_a, procs=4)        # remove dominated solutions
```

Also, remember that `ref_point` is `[1.333, 1.333, ..., 1.333]`. Thus, the file is preprocessed as discussed above but there is a subtle detail. The normalized solutions (`objs_norm`) not in the range `[0, 1]` must be removed. Notice that we used `filter_by_reference_point()` for this purpose. However, we must replace this line:

```python
objs_norm_fil_a = filter_by_reference_point(objs_norm, ref_point) 
```

with this line:

```python
objs_norm_fil_a = filter_by_reference_point(objs_norm, np.ones((m_objs, )))
```

This way, we preserve those normalized solutions within the range `[0, 1]`. 

----

**Remember** to follow these considerations before computing the hypervolume:

- The input must be (1) normalized in `[0, 1]`. This implies that those solutions out of this range must be ignored. 
- The input must contain non-dominated solutions only (this is a requirement for `wfg`).
- The reference point should be `r >= 1`. This is a matter of taste. As discussed in previous posts, some authors prefer to use `r=1.1` regardless the number of objectives; other prefer to use `1` (see `NHV.m` in Platemo); other prefer to use `r = 1 + 1/H`.

----

In other words, my mistake was that, even after normalizing the input solution set, I forgot to remove those points out of the range `[0, 1]`. You can even check the code of `NVH.m` in Platemo:

```matlab
function Score = NHV(PopObj,PF)
% Normalized hypervolume

    [N,M]    = size(PopObj);
    PopObj   = PopObj./repmat(max(PF,[],1)*1.1,N,1);   % normalization (talk about this later)
    PopObj(any(PopObj>1,2),:) = [];                    % remove solutions
    RefPoint = ones(1,M);
```

Notice how they remove solutions out of the range and how the reference point is defined.

Back to our example. The revision `3d085db` of `moead_mss_dci` contains the issue described in this post. We need to use this line to fix it:

```python
# rocket/performance/run_hv_seq.py
objs_norm_fil_a = filter_by_reference_point(objs_norm, np.ones((m_objs, )))
```

The fixed revision is `9e9cfda`.



### Testing the fix

Run the following command again:

```
(dev) auraham@rocket:~/git/hypervolume_guidelines$ python3 run_hv_issue.py 
```

As expected, the hypervolume of the approximation is now slightly inferior than the Pareto front:

```
front_dtlz2_8 INFO     hv_raw: 9.81485116739975005373 hv_norm: 0.98259336123945373309
mss...dtlz2_8 INFO     run_id: 0, hv_raw: 9.80237619214094024755 hv_norm: 0.98134445510100432042
```

Also, the normalized hypervolume (`hv_approximation/hv_front`) now is inferior to `1`:

```
mss...dtlz2_8 INFO     run_id: 0, hv normalized: 0.9987289694926557
```



### TODO

- [ ] Update `run_hv_seq.py` and related python code in `hypervolume_guidelines` to fix the issue
- [ ] Update posts to reflect the fix