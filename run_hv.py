# run_hv.py
# This script shows how to use subprocess for calling wfg as follows:
#
#   proc = subprocess.run(["wfg", "-q", "output/front_mss.txt.filter_norm_both", "1.1", "1.1", "1.1", "-o out.txt"], stdout=subprocess.PIPE)
# 
# that is:
# 
#   wfg -q output/front_mss.txt.filter_norm_both 1.1 1.1 1.1 -o out.txt

import subprocess

if __name__ == "__main__":
    
    """
    proc = subprocess.run(["ls", "-l"], stdout=subprocess.PIPE)
    
    # 1 approach
    output = proc.stdout.decode("utf-8")
    print(output)
    
    # 2 approach
    output = "".join(map(chr, proc.stdout))
    print(output)
    """

    # call wfg
    proc = subprocess.run(["wfg", "-q", "output/front_mss.txt.filter_norm_both", "1.1", "1.1", "1.1", "-o out.txt"], stdout=subprocess.PIPE)

    # get output from wfg
    output = proc.stdout.decode("utf-8")

    hv_raw, hv_norm = output.replace("\n", "").split(" ")
    print("hv_raw: %s hv_norm: %s" % (hv_raw, hv_norm))
    
