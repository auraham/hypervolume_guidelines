# Specification of the reference point

**tl;dr** Use `[1+1/H, 1+1/H, ..., 1+1/H]` as reference point. Otherwise, the hypervolume contribution of the solutions may be different.



## Introduction

In `hypervolume.md`, we specify the reference point as `[1.1, 1.1, ..., 1.1]` for simplicity. Although, the reference point plays a crucial role when computing the hypervolume. If it is too small (i.e., closer to the origin), the hypervolume contribution of extremes solutions will be small or zero. On the other hand, if it is too large (i.e., far from the origin), the hypervolume contribution of the extremes solutions will be larger. 

In [Ishiubchi17] and [Ishibuchi18], the authors provides a simple rule for specifying the reference point so that the hypervolume contribution of each solution is the same.

This post provides a brief summary of these papers, and shows figures to illustrate the idea.



## Reference point specification

In [Ishibuchi17], the authors proposed this convention for specifying the reference point:

> In MOEA/D, the population size $\mu$ is defined for an $m$-objective problem using a user-defined integer parameter $H$ as $\mu = C^{H+m-1}_{m-1}$ where $C^{n}_{m}$ is the number of $m$-combinations from a set of $n$ elements, that i,  $C^{n}_{m} = n!/m!(n-m)!$.[...] in general, the population size $\mu$ is not always specified as $\mu = C^{H+m-1}_{m-1}$. For handling such a general case, first we specify the value of $H$ using the following formulation:
> $$
> C^{H+m-1}_{m-1} \leq \mu < C^{H+m}_{m-1},
> $$
> where $\mu$ is the population size and $m$ is the number of objectives. Then, the reference point $\mathbf{r} = (r, r, \dots, r)$ is specified in the normalized objective space with the ideal point $(0,0, \dots, 0)$ and the Nadir point $(1, 1, \dots, 1)$ as follows:
> $$
> r = 1 + 1 / H
> $$
> Our proposal is to use (1) and (2) (i.e., the two previous equations) for specifying the reference point $\mathbf{r} = (r, r, \dots, r)$ in the normalized objective space from the population size $\mu$ and the number of objectives $m$.

[TODO: provide the intuition of the above paragraph]

Consider five specifications of the reference point for a three-objective problem:

```
r = [1.0, 1.0, 1.0]
r = [1.1, 1.1, 1.1]
r = [1.2, 1.2, 1.2]
r = [1.5, 1.5, 1.5]
r = [2.0, 2.0, 2.0]
```

In the following, we show that the best setting depends on how the population

![](img/fig_contrib_hv_m_3_h_10_point_1.0.png)
![](img/fig_contrib_hv_m_3_h_10_point_1.1.png)
![](img/fig_contrib_hv_m_3_h_10_point_1.2.png)
![](img/fig_contrib_hv_m_3_h_10_point_1.5.png)
![](img/fig_contrib_hv_m_3_h_10_point_2.0.png)


## Results DTLZ1

![](img/fig_contrib_hv_dtlz1_m_3_h_5_point_1.0.png)
![](img/fig_contrib_hv_dtlz1_m_3_h_5_point_1.1.png)
![](img/fig_contrib_hv_dtlz1_m_3_h_5_point_1.2.png)
![](img/fig_contrib_hv_dtlz1_m_3_h_5_point_1.5.png)
![](img/fig_contrib_hv_dtlz1_m_3_h_5_point_2.0.png)

## Results DTLZ3

![](img/fig_contrib_hv_dtlz3_m_3_h_5_point_1.0.png)
![](img/fig_contrib_hv_dtlz3_m_3_h_5_point_1.1.png)
![](img/fig_contrib_hv_dtlz3_m_3_h_5_point_1.2.png)
![](img/fig_contrib_hv_dtlz3_m_3_h_5_point_1.5.png)
![](img/fig_contrib_hv_dtlz3_m_3_h_5_point_2.0.png)

## Results MaF1

![](img/fig_contrib_hv_maf1_m_3_h_5_point_1.0.png)
![](img/fig_contrib_hv_maf1_m_3_h_5_point_1.1.png)
![](img/fig_contrib_hv_maf1_m_3_h_5_point_1.2.png)
![](img/fig_contrib_hv_maf1_m_3_h_5_point_1.5.png)
![](img/fig_contrib_hv_maf1_m_3_h_5_point_2.0.png)

## Results MaF3

![](img/fig_contrib_hv_maf3_m_3_h_5_point_1.0.png)
![](img/fig_contrib_hv_maf3_m_3_h_5_point_1.1.png)
![](img/fig_contrib_hv_maf3_m_3_h_5_point_1.2.png)
![](img/fig_contrib_hv_maf3_m_3_h_5_point_1.5.png)
![](img/fig_contrib_hv_maf3_m_3_h_5_point_2.0.png)


## Results MaF4

![](img/fig_contrib_hv_maf4_m_3_h_5_point_1.0.png)
![](img/fig_contrib_hv_maf4_m_3_h_5_point_1.1.png)
![](img/fig_contrib_hv_maf4_m_3_h_5_point_1.2.png)
![](img/fig_contrib_hv_maf4_m_3_h_5_point_1.5.png)
![](img/fig_contrib_hv_maf4_m_3_h_5_point_2.0.png)

## Results MaF5

![](img/fig_contrib_hv_maf5_m_3_h_5_point_1.0.png)
![](img/fig_contrib_hv_maf5_m_3_h_5_point_1.1.png)
![](img/fig_contrib_hv_maf5_m_3_h_5_point_1.2.png)
![](img/fig_contrib_hv_maf5_m_3_h_5_point_1.5.png)
![](img/fig_contrib_hv_maf5_m_3_h_5_point_2.0.png)





## TODO

- [x] No estoy muy convencido de que el punto de referencia recomendado para maf3 sea lo mejor, ya que la contribucion de los extremos es casi cero.




## References

- [Ishibuchi17] *Reference Point Specification in Hypervolume Calculation for Fair Comparison and Efficient Search.*
- [Ishibuchi18] *How to Specify a Reference Point in Hypervolume Calculation for Fair Performance Comparison.*



