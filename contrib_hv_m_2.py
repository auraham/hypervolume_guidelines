# contrib_hv_m_2.py
# This script replicates figures from 
# [Ishibuchi17] Reference Point Specification in Hypervolume Calculation
# for Fair Comparison and Efficient Search
# specifically
# Fig 3
# 
# Run:
# %run contrib_hv_m_2.py -p maf3

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pylab import rcParams
import os, sys, argparse
from distutils.dir_util import mkpath
from shutil import copy2
import subprocess

# add rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.helpers import save_filtered_objs
from rocket.helpers import filter_by_reference_point
from rocket.helpers import filter_by_nds
from rocket.helpers import normalize

from contrib_hv import hv, contrib_hv
from pareto_fronts import get_front, create_reference_vectors

def load_rcparams(figsize):
    """
    Load a custom rcParams dictionary
    """
    
    # setup figure size
    rcParams['figure.figsize'] = figsize
    
    # style
    #plt.rc('font', family='serif', serif='Times')
    #plt.rc('text', usetex=True)
    #plt.rc('xtick', labelsize=5)
    #plt.rc('ytick', labelsize=5)
    plt.rc('axes', labelsize=18)#, titlesize=9)
    #plt.rc('legend', fontsize=9)

def plot_pop(objs, contrib, title, sf=1, output="fig.png", save=False, show_plot=True):
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    ax.set_title(title)
    ax.set_xlim(0, 1.5)
    ax.set_ylim(0, 1.5)
    
    
    ax.set_xlabel("$f_1$", labelpad=5, rotation=0)
    ax.set_ylabel("$f_2$", labelpad=8, rotation=0)
        

    pop_size, _ = objs.shape

    for i in range(pop_size):
        
        marker="o"
        ms = contrib[i]*sf
        
        # to avoid huge markers
        if contrib[i]*sf > 200:
            marker="s"
            ms = 100
        
        ax.plot(objs[[i], 0], objs[[i], 1], marker=marker, ms=ms, c="#88DC88",
            mec="#252525", ls="none", markeredgewidth=0.15)
   
        ax.plot(objs[[i], 0], objs[[i], 1], marker="x", ms=5, c="#74CE74",
            mec="#252525", ls="none", markeredgewidth=0.25)
    
    # adjust margins
    fig.subplots_adjust(left=0.14,
                        right=0.86,
                        )
                        
    # save figure
    if save:
        fig.savefig(output + ".png", dpi=300)
        fig.savefig(output + ".eps", dpi=300)
        print(output)
    
    if show_plot:
        plt.show()

def str_vector(vec):
    return " ".join(["%.4f" % v for v in vec])

if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--mop_name", required=True, help="mop name")
    args = vars(ap.parse_args())
    
    
    mop_name = args["mop_name"]
    factors = {4: 1000, 5: 2000, 10: 10000, 12: 10000}
    load_rcparams((8, 6))
    save = False
    show_plot = True

    # define h
    for h in (4, ):
        
        # create pop
        m_objs = 2
        objs = get_front(mop_name, m_objs, h)

        # normalize
        z_ideal = objs.min(axis=0)
        z_nadir = objs.max(axis=0)
        objs_norm = normalize(objs, z_ideal, z_nadir)

        # for each ref point
        for point in ("1.0", "1.1", "1.25", "1.5"):
        
            # compute contribution
            ref_point_str = [point] * m_objs
            contrib = contrib_hv(objs_norm, ref_point_str)
            
            # plot
            title = "%s $H = %d$, $r = %s$" % (mop_name.upper(), h, point)
            output = "img/fig_contrib_hv_%s_m_%d_h_%d_point_%s" % (mop_name, m_objs, h, point)
            plot_pop(objs_norm, contrib, title, sf=factors[h], output=output, save=save, show_plot=show_plot)
            
            # debug
            print("mop_name: %s, r: %s, h: %d, contrib: %s" % (mop_name, point, h, str_vector(contrib)))
        
