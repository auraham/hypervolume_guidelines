# Hypervolume indicator and some considerations

**tl;dr** *Do you have a set of points and want to compute this indicator? Good. Proceed as follows: (1) normalize the set of points using the ideal and Nadir points, then filter the set of points by (1) removing those points worsen that the reference point and (2) keeping non-dominated points only. Otherwise, the hypervolume value will be incorrect.*



-----

**Note** We need to specify a reference point for the hypervolume calculation. For simpliciity, we will use `ref_point = [1.1, 1.1, ..., 1.1]` in the examples. We then introduce a better technique to define it as `ref_point = [1+1/H,, 1+1/H, ..., 1+1/H]`.

----



## Introduction

This post gives some guidelines for computing the hypervolume indicator. To this end, we employed a modified version of the implementation from the [walking fish group](link), or WFG. The implementation of the WFG is one of the most employed in the literature. Also, it is fast! (Well, fast enough for up tp $m=10$ objectives). 

We modified that implementation to normalize the hypervolume according to [Cheng]. After compiling the source code, the executable `wfg` is created. The source code is available on [[bitbucket]](https://bitbucket.org/auraham_cg/hypervolume_guidelines).

`wfg` can be used as follows:

```
wfg input_file ref_point -q
```

where:

- `input_file` input text file with the *filtered* set of vectors
- `ref_point` reference point, such as `1.1 ... 1.1`
- `-q` quiet output

The input file has this format:

```
#
0.1 0.2 0.1 ... 0.3
0.2 0.4 0.2 ... 0.2
...
0.3 0.6 0.3 ... 0.1
#
```

where `#` is used to separate fronts in `input_file`. If `input_file` contains a single front, as in this post, then be sure to put a `#` in the first and last line of the file. The number of columns correspond to the number of objectives ($m$). Also, be sure that `reference_point` has $m$ elements.

From the `README.txt` file in the original code:

```
Notes: 

- objective values are separated by spaces; 
- one point per line; 
- fronts are separated by #s; 
- all fronts use the same reference point, therefore all points in all fronts must have the same number of objectives. 

```

and, most important:

```
No error-checking is performed, specifically: 

- in each front, all points are assumed to be mutually non-dominating; 
- for each objective in each front, all values are assumed to lie on the same side of the reference point (i.e. all >= or all <=). 
```

This post describes how to address these two considerations.



## Filter input file by reference point



## Filter input file by non-dominated sorting





## Example #1. A 2-D input matrix







## Example #2. A 3-D input matrix





## Example #3. A *real* case

Now, let us review a full example. For this, we will employ a real output population obtained by MOEA/D using this settings:

- DTLZ3
- $m=8$
- $H=7$
- fev: tchebycheff

The *raw* population is located at `input`:

```
input/objs_moead-m-8-fev-te-dtlz3-h-7_m_8_run_0_final.txt
```

First, compute the hypervolume for this file as follows:

```
$ cd input
$ wfg objs_moead-m-8-fev-te-dtlz3-h-7_m_8_run_0_final.txt 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 10, ind_filename: 1, ind_ref_vect: 2
filename: objs_moead-m-8-fev-te-dtlz3-h-7_m_8_run_0_final.txt
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
3, 1.10 
4, 1.10 
5, 1.10 
6, 1.10 
7, 1.10 
Segmentation fault (core dumped)
```

This segmentation fault is caused because the input file does not contains `#` as separators. Add these separator at the start and end of the file. The resulting file is called `pop_raw.txt`:

```
$ wfg pop_raw.txt 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 10, ind_filename: 1, ind_ref_vect: 2
filename: pop_raw.txt
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
3, 1.10 
4, 1.10 
5, 1.10 
6, 1.10 
7, 1.10 
# fronts: 1
# maxm: 3432
# maxn: 8
527.028065 245.862482
Time: 0.328041 (s)
Total time = 0.328041 (s)
```

Apparently, the hypervolume is `527.0028065`, and `245.862482` after normalization. *How could it be?* This is because some of the points are larger than the reference point `[1.1, 1.1, ..., 1.1]`. Let's review `pop_raw.txt`:

```
In [1]: pop = np.genfromtxt("pop_raw.txt")                     
In [2]: pop.max(axis=0) 
Out[2]: 
array([26.33012122,  0.9605815 , 23.90147508,  1.22462085,  1.00145893,
        1.00060682,  1.00024522,  1.00043968])
```

In the following, we remove points higher/larger than the reference point. To do so,  we use `filter_pop.py` as follows:

```
(dev) auraham@rocket:~/git/hypervolume_guidelines$ mkdir output
(dev) auraham@rocket:~/git/hypervolume_guidelines$ python3 filter_pop.py 
filter 1: completed
filter 2: completed
filter 3: completed
```

This script creates three output files:

- `pop.txt.filter_ref_point`
- `pop.txt.filter_nds`
- `pop.txt.filter_both`

After creating these files, compute their hypervolume as follows:

**Filter 1** Filter by reference point only. The normalized hypervolume is `0.799851`. It seems correct.

```
$ wfg pop.txt.filter_ref_point 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 10, ind_filename: 1, ind_ref_vect: 2
filename: pop.txt.filter_ref_point
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
3, 1.10 
4, 1.10 
5, 1.10 
6, 1.10 
7, 1.10 
# fronts: 1
# maxm: 3406
# maxn: 8
1.714551 0.799851
Time: 0.392629 (s)
Total time = 0.392629 (s)
```

**Filter 2** Filter by non-dominated sorting only. The normalized hypervolume is `1.189051`. It seems strange!

```
$ wfg pop.txt.filter_nds 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 10, ind_filename: 1, ind_ref_vect: 2
filename: pop.txt.filter_nds
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
3, 1.10 
4, 1.10 
5, 1.10 
6, 1.10 
7, 1.10 
# fronts: 1
# maxm: 2160
# maxn: 8
2.548837 1.189051
Time: 0.261016 (s)
Total time = 0.261016 (s)
```

**Filter 3** Both filters are applied. The normalized hypervolume is `0.799851`, the same value obtained by filter 1.

```
wfg pop.txt.filter_both 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 10, ind_filename: 1, ind_ref_vect: 2
filename: pop.txt.filter_both
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
3, 1.10 
4, 1.10 
5, 1.10 
6, 1.10 
7, 1.10 
# fronts: 1
# maxm: 2137
# maxn: 8
1.714551 0.799851
Time: 0.337788 (s)
Total time = 0.337788 (s)
```

Note that both first and last filters returned the same results. Maybe the populations are the same. Let's compare them. Run `plot_pops.py` to create the following plots:

![](img/all_pops.png)

This figure shows the three filtered populations. Observe that the second population (filtered using NDS only) contains solutions out of the range `[0, 1.1]` (i.e., solutions worsen that the reference point). For that reason, the hypervolume of this population is not reliable (`2.548837 1.189051` as you remember). This population is shown alone in the next plot:

![](img/pop_b.png)

The next figure compares the two other populations, one filtered using the reference point and the other one filtered using the reference point and NDS. The hypervolume of both populations is the same:

```
1.714551 0.799851
1.714551 0.799851
```



![](img/pop_a_c.png)



However, if we visually inspect both populations, we can see that they differ. We can see that the first population (`Filter ref point`) contains more solutions. In fact, the number of solutions in both files is different:

```
(dev) auraham@rocket:~/git/hypervolume_guidelines/output$ wc -l pop.txt.filter_ref_point
3408 pop.txt.filter_ref_point
(dev) auraham@rocket:~/git/hypervolume_guidelines/output$ wc -l pop.txt.filter_both 
2139 pop.txt.filter_both
```



## Example #4. A *real* case considering normalization

In the previous example, we compute the hypervolume of a solution set as follows:

1. Filter the input population using the reference point (i.e., those points that are not in the range `[0, 1.1]` are removed).
2. Filter again using NDS (i.e., dominated solutions are removed).

In this case, we will consider an additional step:

0. Normalize the input population in the range `[0, 1]` using `z_ideal` and `z_nadir`.

Let's add this block in `filter_pop.py`:

```python
# filter 4: normalization + ref point + nds (first front)
z_ideal = np.zeros((m_objs, ))
z_nadir = np.ones((m_objs, ))
objs_n = normalize(objs, z_ideal, z_nadir)
objs_c = filter_by_reference_point(objs_n, ref_point)
objs_d = par_filter_pop(objs_c, procs=12)
save_filtered_objs(objs_d, output_filepath + ".filter_norm_both")
print("filter 4: completed")
```

In this case, we normalize `objs` before performing both filters. The output is located in:

```
output/pop.txt.filter_norm_both
```

However, in this case normalizing the input population does not change the outcome of the filters. Check this command:

```
(dev) auraham@rocket:~/git/hypervolume_guidelines/output$ colordiff pop.txt.filter_both pop.txt.filter_norm_both
```

There is no difference among both files. This is because we use `z_nadir = [1,1,...,1]` for normalizing the population. In the next example, we use a different setting for `z_nadir`, meaning that the outcome of the filters will change. In summary, we recommend this approach for computing the hypervolume indicator:

0. Normalize the input population in the range `[0, 1]` using `z_ideal` and `z_nadir`.
1. Filter the input population using the reference point (i.e., those points that are not in the range `[0, 1.1]` are removed).
2. Filter again using NDS (i.e., dominated solutions are removed).
3. Compute the hypervolume indicator.





## Example #5. A badly-scaled problem

In this example, we compute the hypervolume of a population obtained after solving a problem called MaF4. This problem is badly-scaled, meaning that the range of the objective functions is not [0, 1]. Instead, the range of the `i`-th objective of MaF4 is `[0, 2**i]` . For instance, the Nadir point for a three-objective instance of MaF4 is `z_nadir=[2, 4, 8]`.

Let's add this block into `filter_pop.py`:

```python
# load input file
input_filepath  = "input/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_1_t_500_fev_ws.txt"
output_filepath = "output/pop_mss.txt"
output_front_filepath = "output/front_mss.txt"

# load objs
objs = np.genfromtxt(input_filepath)

# size
pop_size, m_objs = objs.shape

# load real front
mop_name = "maf4"
front = get_front(mop_name, m_objs)

# define z_ideal and z_nadir
z_ideal = front.min(axis=0)
z_nadir = front.max(axis=0)

print("z_ideal:", str_vector(z_ideal))
print("z_nadir:", str_vector(z_nadir))

# ref point
# use [1.1, 1.1, ..., 1.1] if the population 
# is normalized in the range [0, 1]
ref_point = np.array([1.1] * m_objs)

# --- debug ---
pops = (front, objs)
lbls = ("front", "objs")
plot_pops(pops, lbls)

# filter: normalization + ref point + nds (first front)
front_n = normalize(front, z_ideal, z_nadir)
objs_n = normalize(objs, z_ideal, z_nadir)
objs_c = filter_by_reference_point(objs_n, ref_point)
objs_d = par_filter_pop(objs_c, procs=4)
save_filtered_objs(objs_d, output_filepath + ".filter_norm_both")           # save pop
save_filtered_objs(front_n, output_front_filepath + ".filter_norm_both")    # save front
print("filter: completed")
```

Now, let's explain each part. First, we define some paths for loading and saving results:

```python
# load input file
input_filepath  = "input/objs_mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true_m_3_run_1_t_500_fev_ws.txt"
output_filepath = "output/pop_mss.txt"
output_front_filepath = "output/front_mss.txt"
```

Next, we load the input population and the real front of MaF4. We then define `z_ideal` and `z_nadir` from `front`:

```python
# load objs
objs = np.genfromtxt(input_filepath)

# size
pop_size, m_objs = objs.shape

# load real front
mop_name = "maf4"
front = get_front(mop_name, m_objs)

# define z_ideal and z_nadir
z_ideal = front.min(axis=0)
z_nadir = front.max(axis=0)
```

Since `objs` and `front` will be normalized, we define the reference vector for the hypervolume as follows:

```python
# ref point
# use [1.1, 1.1, ..., 1.1] if the population 
# is normalized in the range [0, 1]
ref_point = np.array([1.1] * m_objs)
```

Finally, we normalize `front` and `objs`, and filter `objs`. The outputs are stored within `output`:

```python
# filter: normalization + ref point + nds (first front)
front_n = normalize(front, z_ideal, z_nadir)
objs_n = normalize(objs, z_ideal, z_nadir)
objs_c = filter_by_reference_point(objs_n, ref_point)
objs_d = par_filter_pop(objs_c, procs=4)
save_filtered_objs(objs_d, output_filepath + ".filter_norm_both")           # save pop
save_filtered_objs(front_n, output_front_filepath + ".filter_norm_both")    # save front
print("filter: completed")
```

These figures are generated after running `filter_pop.py`:

![](img/fig_maf4_before_normalization.png)





![](img/fig_maf4_after_normalization.png)



Observe that the population and front in the last plot are normalized.



Now, we can compute the hypervolume of the population:

```
wfg pop_mss.txt.filter_norm_both 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 5, ind_filename: 1, ind_ref_vect: 2
filename: pop_mss.txt.filter_norm_both
# fronts: 1
# maxm: 91
# maxn: 3
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
0.689151 0.517769
Time: 0.000216 (s)
Total time = 0.000216 (s)
```

and the Pareto front:

```
wfg front_mss.txt.filter_norm_both 1.1 1.1 1.1
verbose flag: 1, arg_index: 1, argc: 5, ind_filename: 1, ind_ref_vect: 2
filename: front_mss.txt.filter_norm_both
# fronts: 1
# maxm: 91
# maxn: 3
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
0.706498 0.530802
Time: 0.000196 (s)
Total time = 0.000196 (s)
```

As it can be seen, the hypervolume of the Pareto front is slightly better than the population:

```python
# population
0.689151 0.517769

# pareto front
0.706498 0.530802
```



 

## TODO

- [ ] Modificar `wfg` para manipular archivos de entrada como este

```
#
#
```

ahora, no marca error (quizá por la bandera `-q`), pero devuelve un hypervolumen ruidoso, algo como:

```
1188114359143434496.000000 609690528780926464.000000
```

- [ ] Add examples for `pop_2d.txt`, `pop_3d.txt`
- [ ] Add `plot_pop.py`, `load_rcparams`, `colors`
- [ ] Add `plot_pop(objs..)` in `__main__`
- [ ] Save `png` and put them here





## TODO

- [ ] Add normalization of the input objs
- [ ] Revisar que pasa cuando los objs son de dtlz3 (un problema escalado) son normalizados, es decir, que pasa cuando la matriz de entrada tiene esta forma:

```
1.10 1.20 0.90
0.90 0.95 0.98
0.92 0.97 1.10   # que pasa con los puntos que se pasan del rango [0, 1]
```

creo que este debe ser el contenido de la poblacion despues de la normalizacion

```
1.10 1.20 0.90
0.90 0.95 0.98
0.92 0.97 1.10
```

es decir, no debe cambiar

- [ ] Cambiar los imports de este tipo para que el repositorio sea autocontenido:

```python
# replace this block
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")
from rocket.pareto_fronts import get_front
from rocket.plot import load_rcparams, plot_pops, parallel_coordinates
```

- [ ] Calcular el hv usando la convencion de Ishibuchi, `[1 + 1/H, 1 + 1/H, ..., 1 + 1/H]`
- [ ] Copiar parte de este repo en `python_call_external_command` o hacerlo aqui
- [ ] Comparar la salida con la de `run_hv_m_3.sh`





## References